﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupMovement : MonoBehaviour
{
    [SerializeField] private float BounceSpeed;
    [SerializeField] private float BounceHeight;
    [SerializeField] private float RotationSpeed;

    private bool up = true;
    private float startY;

    private void Start()
    {
        startY = transform.position.y;
    }

    void Update()
    {
        transform.Rotate(new Vector3(0, 1, 0), RotationSpeed * Time.deltaTime);

        Vector3 newPos = transform.position;
        if (up)
        {
            newPos.y += BounceSpeed * Time.deltaTime;
            if (newPos.y > startY + BounceHeight)
            {
                up = false;
                newPos.y = startY + BounceHeight;
            }
        }
        else
        {
            newPos.y -= BounceSpeed * Time.deltaTime;
            if (newPos.y < startY)
            {
                up = true;
                newPos.y = startY;
            }
        }

        transform.position = newPos;
    }
}
