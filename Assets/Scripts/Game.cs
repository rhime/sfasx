﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Audio;

using TMPro;

public class Game : MonoBehaviour
{
    [SerializeField] private Camera MainCamera;
    [SerializeField] private CameraMovement CameraController;
    [SerializeField] private Portal Portal;
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private GameObject LoadButton;
    [SerializeField] private GameObject ContinueButton;
    [SerializeField] private ImageFade FadeEffect;
    [SerializeField] private GameObject DeathText;
    [SerializeField] private TextMeshProUGUI FloorText;

    [SerializeField] private GameObject MapCamera;
    [SerializeField] private GameObject MapImage;

    [SerializeField] private Player mCharacter;
    private Environment mMap;

    // Input checks
    private const float DoubleClickTime = 0.8f;
    private int clicks = 0;
    private float timeSinceClick;

    private const float KeyPressCooldown = 0.2f;
    private float timeSinceKeyPress = 0;

    // Times between path checks while holding mouse button, also used to limit door toggles
    private const float RefreshTime = 0.4f;
    private float timeSinceRefresh = 0.0f;

    // Time before moving to the next floor
    [SerializeField] private float TeleportTime = 2.5f;
    private float teleportProgress = 0.0f;

    // Progression data
    [SerializeField] private int CurrentFloor = 1;

    // Player cannot move while it's true
    private bool isPaused = false;

    // Time before the black screen fades out
    [SerializeField] private float FadeBreak = 0.5f;

    // Game saves
    private string SavePath;

    // Player skills
    public enum EPlayerSkill
    {
        MagicMissile = 0,
        Fireball = 1,
        Void = 2,
        Shield = 3
    }
    private EPlayerSkill selectedAttack = EPlayerSkill.MagicMissile;
    private List<float> SkillCost = new List<float> {0, 20, 60, 80};
    private List<float> SkillLevel = new List<float> { 0, 2, 8, 18 };
    private List<float> SkillUpgradeLevel = new List<float> { 5, 13, 22, 26 };
    [SerializeField] private List<Button> SkillButtons;
    [SerializeField] private List<Image> SkillSelection;
    [SerializeField] private List<GameObject> SkillCanvas;
    [SerializeField] private List<Sprite> SkillImage;
    [SerializeField] private List<Sprite> SkillUpgradeImage;
    private List<bool> skillUpgraded = new List<bool> { false, false, false, false };

    // Sound
    [SerializeField] private AudioMixer SoundMixer;
    [SerializeField] private AudioSource Audio;
    [SerializeField] private AudioClip DeathSound;

    [SerializeField] private List<Button> SoundButton;
    [SerializeField] private List<Sprite> SoundImageOn;
    [SerializeField] private List<Sprite> SoundImageOff;

    private List<float> SoundDefault = new List<float> { -80, 0 };
    private List<float> MusicDefault = new List<float> { -80, -20 };
    private List<string> SoundParam = new List<string> { "SoundVolume", "MusicVolume" };

    private List<int> SoundOn;

    void Start()
    {
        mMap = GetComponentInChildren<Environment>();

        SavePath = Application.persistentDataPath + "/meow1.save";

        ShowMenu(true);

        // Sound player prefs
        SoundOn = new List<int> { 1, 1 };

        for (int i = 0; i < SoundParam.Count; i++)
        {
            if (!PlayerPrefs.HasKey(SoundParam[i])) PlayerPrefs.SetInt(SoundParam[i], 1);
            else if (PlayerPrefs.GetInt(SoundParam[i]) == 0) ToggleSound(i);
        }
    }

    private void Update()
    {
        if (!isPaused)
        {
            UpdateSkillAvailibility();
            MouseControl();
            KeyboardControl();

            // Check if player is on an active goal tile
            if (mMap.GoalTile != null && mCharacter.CurrentPosition != null && Portal.isActive && (mCharacter.transform.position - mMap.GoalTile.Position).sqrMagnitude <= 25)
            {
                teleportProgress += Time.deltaTime;
            }
            else
            {
                teleportProgress = 0.0f;
            }

            Portal.ToggleStars(teleportProgress > 0.0f);

            // After standing on the goal tile long enough load next floor
            if (teleportProgress > TeleportTime)
            {
                StartCoroutine(Teleport());
            }
        }
    }

    public void SkillSetup()
    {
        // Show icons of the unlocked/upgraded skills
        for (int i = 0; i < SkillCanvas.Count; i++)
        {
            SkillCanvas[i].SetActive(mCharacter.CharacterLevel >= SkillLevel[i]);
            skillUpgraded[i] = SkillUpgradeLevel[i] > 0 && mCharacter.CharacterLevel >= SkillUpgradeLevel[i];
            SkillButtons[i].image.sprite = skillUpgraded[i] ? SkillUpgradeImage[i] : SkillImage[i];
        }
    }

    private void UpdateSkillAvailibility()
    {
        // Check if player can cast the skills
        for (int i = 0; i < SkillButtons.Count; i++)
        {
            bool available = mCharacter.CurrentMP >= SkillCost[i];
            SkillButtons[i].interactable = available;
            if (!available && (int)selectedAttack == i)
            {
                selectedAttack = EPlayerSkill.MagicMissile;
                SkillSelection[i].gameObject.SetActive(false);
                SkillSelection[0].gameObject.SetActive(true);
            }
        }
    }

    public void SkillClick(int skillNo)
    {
        EPlayerSkill skill = (EPlayerSkill)skillNo;

        // Selectable skills
        if (skill != selectedAttack && skillNo <= 2)
        {
            // Show selection
            SkillSelection[(int)selectedAttack].gameObject.SetActive(false);
            SkillSelection[skillNo].gameObject.SetActive(true);

            selectedAttack = skill;
        }
        // One use skills
        else if (skillNo == 3)
        {
            mCharacter.CastSkill(skill, SkillCost[skillNo], null, skillUpgraded[skillNo]);
        }
    }

    private IEnumerator Teleport()
    {
        Debug.Log("Teleporting to next floor.");

        // Pause character control
        isPaused = true;
        mCharacter.StopAllCoroutines();
        mCharacter.Idle();

        // Fade in and get a new floor
        FadeEffect.FadeIn();

        yield return new WaitUntil(FadeEffect.IsPaused);
        yield return new WaitForSeconds(FadeBreak);

        CurrentFloor++;
        Generate();

        FadeEffect.FadeOut();
        yield return new WaitUntil(FadeEffect.IsPaused);

        isPaused = false;
        yield return null;
    }

    private void MouseControl()
    {
        // Check number of clicks
        if (Input.GetMouseButtonDown(0)) clicks++;
        if (clicks > 0 && !Input.GetKey(KeyCode.Mouse0)) timeSinceClick += Time.deltaTime;

        if (timeSinceClick > DoubleClickTime)
        {
            clicks = 0;
            timeSinceClick = 0;
        }
  
        timeSinceRefresh += Time.deltaTime; // This limits the number of path searches and door toggles

        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                // Cast a ray from camera to the mouse click position
                Ray screenClick = MainCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitPoint;

                // Check what the ray hit
                if (Physics.Raycast(screenClick, out hitPoint, Mathf.Infinity))
                {
                    // If a monster was clicked use the selected attack on it
                    if (hitPoint.collider.tag == "RaycastMonster" || hitPoint.collider.tag == "Monster")
                    {
                        GameObject target = hitPoint.collider.GetComponentInParent<Monster>().gameObject;

                        if (target.GetComponent<Monster>().monsterState != Monster.EMonsterState.Dead) 
                            mCharacter.CastSkill(selectedAttack, SkillCost[(int)selectedAttack], target, skillUpgraded[(int)selectedAttack]);
                    }
                    // Check to see if the player has clicked a tile and if they have, try to find a path to that 
                    // tile. If we find a path then the character will move along it to the clicked tile. 
                    else if (hitPoint.collider.tag == "Floor" && (clicks == 2 || (clicks > 0 && timeSinceRefresh > RefreshTime)))
                    {
                        timeSinceRefresh = 0.0f;

                        if (clicks == 2) clicks++; // Time limiter is ignored when registering a second click, after that click count is increased to use the limiter again

                        EnvironmentTile tile = hitPoint.collider.GetComponentInParent<EnvironmentTile>();
                        if (tile != null)
                        {
                            List<EnvironmentTile> route = mMap.Solve(mCharacter.CurrentPosition, tile);
                            if (route == null) route = mMap.GoNearTarget(mCharacter.CurrentPosition, tile); // Go to the nearest tile if the one clicked is inaccessible
                            mCharacter.GoTo(route, clicks > 1);
                        }
                    }
                    // Check if a door was clicked. I the player is close to it, open or close the door.
                    else if (hitPoint.collider.tag == "Door" && (clicks > 0 && timeSinceRefresh > RefreshTime))
                    {
                        timeSinceRefresh = 0.0f;

                        Door door = hitPoint.collider.GetComponentInParent<Door>();

                        if (mCharacter.CurrentPosition != door.Tile && Vector3.Distance(door.transform.position, mCharacter.transform.position) <= door.ToggleDistance)
                        {
                            mCharacter.transform.LookAt(door.transform);
                            door.Toggle();
                        }
                    }
                }
            }
        }
    }
    private void KeyboardControl()
    {
        timeSinceKeyPress += Time.deltaTime; // Limits key presses

        if (Input.anyKeyDown && timeSinceKeyPress >= KeyPressCooldown)
        {
            timeSinceKeyPress = 0;

            // Skills
            if (Input.GetKey(KeyCode.Alpha1) && SkillButtons[0].interactable)
            {
                SkillClick(0);
            }
            else if (Input.GetKey(KeyCode.Alpha2) && SkillButtons[1].interactable)
            {
                SkillClick(1);
            }
            else if (Input.GetKey(KeyCode.Alpha3) && SkillButtons[2].interactable)
            {
                SkillClick(2);
            }
            else if (Input.GetKey(KeyCode.Q) && SkillButtons[2].interactable)
            {
                SkillClick(3);
            }
            // Menu
            else if (Input.GetKey(KeyCode.Escape))
            {
                ShowMenu(true);
            }
            // Map
            else if (Input.GetKey(KeyCode.M))
            {
                ToggleMap();
            }
            // Pause in debug mode
            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Break();
            }
        }
    }

    public void PlayerDeath()
    {
        // Coroutine has to be called from the game object directly
        StartCoroutine(RestartGame());
    }

    public void UpdatePlayerExp()
    {
        // Called from here to prevent the coroutine from getting stopped
        StartCoroutine(mCharacter.UpdateExp());
    }

    private IEnumerator RestartGame()
    {
        Debug.Log("Player died.");

        // Stop character control
        isPaused = true;
        mCharacter.StopAllCoroutines();
        mCharacter.Idle();

        // Play death sound and fade in
        SoundMixer.SetFloat(SoundParam[1], MusicDefault[0]);
        if (Audio != null && DeathSound != null) Audio.PlayOneShot(DeathSound);

        FadeEffect.FadeIn();

        // Show death text and wait till player clicks on screen before restarting the floor
        DeathText.SetActive(true);
        yield return new WaitUntil(FadeEffect.IsPaused);

        mMap.CleanUpWorld();

        yield return new WaitUntil(() => Input.GetKey(KeyCode.Mouse0));
        DeathText.SetActive(false);

        if (SoundOn[1] == 1) SoundMixer.SetFloat(SoundParam[1], MusicDefault[1]);

        Debug.Log("Reloading floor");
        LoadGame();

        FadeEffect.FadeOut();

        isPaused = false;
        Debug.Log("Resuming game.");
        yield return null;
    }

    public void ShowMenu(bool show)
    {
        if (Menu != null && Hud != null)
        {
            if (show)
            {
                mCharacter.Idle();
                mCharacter.StopAllCoroutines();
            }

            isPaused = show;

            Menu.enabled = show;
            Hud.enabled = !show;

            LoadButton.SetActive(File.Exists(SavePath)); // Visible if a save file exists
            ContinueButton.SetActive(mMap.StartTile != null); // Visible after starting the game
        }
    }

    public void ToggleMap()
    {
        // Show/hide the map
        bool active = !MapCamera.activeSelf;
        MapCamera.SetActive(active);
        MapImage.SetActive(active);
    }

    public void ToggleSound(int which)
    {
        SoundOn[which] = SoundOn[which] == 0 ? 1 : 0; // Toggle sound value

        UpdateSoundIcons(which);

        SoundMixer.SetFloat(SoundParam[which], which == 0 ? SoundDefault[SoundOn[0]] : MusicDefault[SoundOn[1]]); // Turn the sound on or off

        PlayerPrefs.SetInt(SoundParam[which], SoundOn[which]); // Save preference
    }

    private void UpdateSoundIcons(int which)
    {
        SoundButton[which].image.sprite = SoundOn[which] == 1 ? SoundImageOn[which] : SoundImageOff[which];
    }

    public void NewGame()
    {
        // Reset all progress and update UI
        CurrentFloor = 1;
        mMap.CurrentFloor = 1;

        mCharacter.CharacterLevel = 1;
        mCharacter.AttackLevel = 1;
        mCharacter.DefenseLevel = 1;

        mCharacter.SetRequiredExp();
        mCharacter.CurrentExp = 0;
        mCharacter.GetExp(0);
        mCharacter.UpdateLevelText();
        SkillSetup();
        SkillClick(0);

        mCharacter.AttackUp(0);
        mCharacter.DefenseUp(0);

        // Generate a new floor
        Generate();
    }

    public void Generate()
    {
        FloorText.text = CurrentFloor.ToString();

        mMap.GenerateWorld(CurrentFloor);

        SaveGame();

        mMap.BuildWorld();
        mCharacter.CurrentPosition = mMap.StartTile;
        mCharacter.transform.position = mMap.StartTile.Position;
        CameraController.ResetPosition();

        mCharacter.CurrentHP = mCharacter.MaxHP;
        mCharacter.CurrentMP = mCharacter.MaxMP;
        mCharacter.CurrentArmor = 0;

        mCharacter.UpdateHPBar(true);
        mCharacter.UpdateMPBar(true);
        mCharacter.UpdateArmorBar(true);
    }
    private void SaveGame()
    {
        Save save = new Save();

        save.currentFloor = CurrentFloor;
        save.playerLevel = mCharacter.CharacterLevel;
        save.playerAttackLevel = mCharacter.AttackLevel;
        save.playerDefenseLevel = mCharacter.DefenseLevel;
        save.playerExp = mCharacter.CurrentExp;

        save.map = mMap.tileMap;
        save.rooms = mMap.roomsCreated;

        save.taskType = mMap.Task.type;
        save.requirement = mMap.Task.requirement;

        save.SaveGame(SavePath);

        Debug.Log("Game saved to " + Application.persistentDataPath + SavePath);
    }

    public void LoadGame()
    {
        Save save = new Save();
        save = save.LoadGame(SavePath);

        CurrentFloor = save.currentFloor;
        mMap.CurrentFloor = CurrentFloor;

        mCharacter.CharacterLevel = save.playerLevel;
        mCharacter.AttackLevel = save.playerAttackLevel;
        mCharacter.DefenseLevel = save.playerDefenseLevel;

        mCharacter.SetRequiredExp();
        mCharacter.CurrentExp = save.playerExp;
        mCharacter.GetExp(0);
        mCharacter.UpdateLevelText();
        SkillSetup();

        mMap.tileMap = save.map;
        mMap.roomsCreated = save.rooms;

        mMap.Task.LoadTask(save.taskType, save.requirement);

        mMap.BuildWorld();
        FloorText.text = CurrentFloor.ToString();

        mCharacter.transform.position = mMap.StartTile.Position;
        mCharacter.CurrentPosition = mMap.StartTile;
        CameraController.ResetPosition();

        mCharacter.AttackUp(0);
        mCharacter.DefenseUp(0);
        mCharacter.CurrentHP = mCharacter.MaxHP;
        mCharacter.CurrentMP = mCharacter.MaxMP;
        mCharacter.CurrentArmor = 0;
        mCharacter.UpdateHPBar(true);
        mCharacter.UpdateMPBar(true);
        mCharacter.UpdateArmorBar(true);

        Debug.Log("Game loaded");
    }

    public void Exit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }
}
