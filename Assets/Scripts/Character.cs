﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    // Movement
    [SerializeField] protected float WalkingSpeed = 12.0f;
    [SerializeField] protected float RunningSpeed = 30.0f;
    protected float SpeedMultiplier = 1;
    protected const float SplinePoints = 8;

    public EnvironmentTile CurrentPosition { get; set; }

    // Animation
    [SerializeField] protected Animator animator;

    [SerializeField] protected float AnimatorIdleSpeed = 1.0f;
    [SerializeField] protected float AnimatorWalkSpeed = 1.8f;
    [SerializeField] protected float AnimatorRunSpeed = 1.2f;

    public bool isRunning = false;

    // Stats and status
    [SerializeField] protected bool Immortal = false;

    [SerializeField] protected UISetBarFill HPBar;
    [SerializeField] protected UISetBarFill MPBar;
    [SerializeField] protected UISetBarFill ArmorBar;
    protected ShowDamage DamageText;

    [SerializeField] protected Vector2Int MaxHPRange;
    public float MaxHP;
    public float CurrentHP;
    public float CurrentArmor = 0;

    public float MaxMP = 100;
    public float CurrentMP = 0;

    public Vector2Int Attack;
    public Vector2Int Defense;

    protected float HPCoefficient = 0.018f;
    protected float AttackCoefficient = 0.06f;
    protected float DefenseCoefficient = 0.0035f;

    public int CharacterLevel = 1;
    public int AttackLevel = 1;
    public int DefenseLevel = 1;

    // Auto-recovery
    [SerializeField] protected float RecoveryRate = 1.5f;
    [SerializeField] protected Vector2 RecoveryPotency = new Vector2(0.05f, 0.1f);
    [SerializeField] protected float RecoveryDelay = 15.0f;
    protected float lastHit;
    protected float lastRecovery;

    [SerializeField] protected float MPRecoveryRate = 1.5f;
    [SerializeField] protected Vector2 MPRecoveryPotency = new Vector2(0, 0);
    protected float lastMPRecovery;

    // Skills 
    [SerializeField] protected Attack AttackSpawn;

    // Sound
    [SerializeField] protected AudioSource Audio;
    [SerializeField] protected AudioClip HitSound;
    [SerializeField] protected AudioClip StepSoundWalk;
    [SerializeField] protected AudioClip StepSoundRun;
    [SerializeField] protected float StepFrequencyWalk = 1;
    [SerializeField] protected float StepFrequencyRun = 1;
    protected float stepTimer = 0;

    protected enum EMovement
    {
        Idle,
        Walk,
        Run
    }
    protected EMovement movementState = EMovement.Idle;

    private void Start()
    {
        Idle();

        DamageText = GameObject.FindObjectOfType<ShowDamage>();

        lastHit = RecoveryDelay;
        lastRecovery = RecoveryRate;

        lastMPRecovery = MPRecoveryRate;
    }

    private void Update()
    {
        BattleUpdate();
        CustomUpdate();
        StepSoundUpdate();
    }

    protected virtual void CustomUpdate() { }

    // Basic animation 
    public virtual void Idle()
    {
        isRunning = false;

        if (movementState != EMovement.Idle)
        {
            movementState = EMovement.Idle;

            animator.SetTrigger("Idle");
        }
        animator.speed = AnimatorIdleSpeed;
    }
    protected void Walk()
    {
        isRunning = false;
        if (movementState != EMovement.Walk)
        {
            movementState = EMovement.Walk;
            animator.SetTrigger("Walk");
        }
        animator.speed = AnimatorWalkSpeed;
    }
    protected void Run()
    {
        isRunning = true;
        if (movementState != EMovement.Run)
        {
            movementState = EMovement.Run;
            animator.SetTrigger("Run");
            animator.speed = AnimatorRunSpeed;
        }

        animator.speed = AnimatorRunSpeed;
    }

    // Movement
    protected IEnumerator DoMove(Vector3 position, Vector3 destination)
    {
        // Move between the two specified positions over the specified amount of time
        if (position != destination)
        {
            transform.rotation = Quaternion.LookRotation(destination - transform.position, Vector3.up);

            Vector3 p = transform.position;
            float t = 0.0f;

            float distance = (position - destination).magnitude;
            float speedFactor = (isRunning ? RunningSpeed / distance : WalkingSpeed / distance) * SpeedMultiplier;

            while (transform.position != destination)
            {
                t += Time.deltaTime * speedFactor;
                p = Vector3.Lerp(position, destination, t);
                transform.position = p;
                yield return null;
            }
        }
    }

    protected IEnumerator DoGoTo(List<EnvironmentTile> route)
    {
        // Move through each tile in the given route
        if (route != null && route.Count > 1)
        {
            List<Vector3> splinePath = SplinePath(route);
            Vector3 position = transform.position;

            for (int count = 1; count < splinePath.Count; ++count)
            {
                Vector3 next = splinePath[count];
                yield return DoMove(position, next);
                position = next;
            }
        }
        Idle();
    }

    public void GoTo(List<EnvironmentTile> route, bool fast = false)
    {
        if (route != null && route[route.Count - 1] != CurrentPosition)
        {
            // Clear all coroutines before starting the new route so 
            // that clicks can interupt any current route animation
            StopAllCoroutines();

            // Set animation
            isRunning = fast;
            if (isRunning) Run();
            else Walk();

            StartCoroutine(DoGoTo(route));
        }
    }

    private void StepSoundUpdate()
    {
        if (movementState != EMovement.Idle && Audio != null && StepSoundRun != null && StepSoundWalk != null)
        {
            stepTimer += Time.deltaTime;
            if (stepTimer > (isRunning ? StepFrequencyRun : StepFrequencyWalk))
            {
                Audio.PlayOneShot(isRunning ? StepSoundRun : StepSoundWalk);
                stepTimer = 0;
            }
        }
    }

    // Path smoothing
    protected Vector3 GetSplinePoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        Vector3 splinePoint = transform.position;
        splinePoint.x = (Mathf.Pow(1 - t, 3) * p0.x) + (3 * t * Mathf.Pow(1 - t, 2) * p1.x) + (3 * Mathf.Pow(t, 2) * (1 - t) * p2.x) + (Mathf.Pow(t, 3) * p3.x);
        splinePoint.z = (Mathf.Pow(1 - t, 3) * p0.z) + (3 * t * Mathf.Pow(1 - t, 2) * p1.z) + (3 * Mathf.Pow(t, 2) * (1 - t) * p2.z) + (Mathf.Pow(t, 3) * p3.z);

        return splinePoint;
    }

    protected List<Vector3> SplinePath(List<EnvironmentTile> path)
    {
        // Smooth out the given path using Bézier spline
        List<Vector3> splinePath = new List<Vector3>();

        Vector3 p0;
        Vector3 p1;
        Vector3 p2;
        Vector3 p3;

        splinePath.Add(path[0].Position);
        for (int i = 1; i < path.Count - 1; i++)
        {
            if (i == 0) p0 = path[0].Position;
            else p0 = path[i - 1].Position;
            p1 = path[i].Position;
            p2 = path[i + 1].Position;
            if (i == path.Count - 2) p3 = path[i + 1].Position;
            else p3 = path[i + 2].Position;

            for (float j = 0.5f; j < 0.75f; j += (1.0f / SplinePoints))
            {
                splinePath.Add(GetSplinePoint(p0, p1, p2, p3, j));
            }
        }
        splinePath.Add(path[path.Count - 1].Position);

        return splinePath;
    }

    // Battle
    private void BattleUpdate()
    {
        // Recover HP if out of battle
        lastHit += Time.deltaTime;
        lastRecovery += Time.deltaTime;

        if (lastHit > RecoveryDelay && CurrentHP < MaxHP && CurrentHP > 0)
        {
            if (lastRecovery > RecoveryRate)
            {
                lastRecovery = 0;
                int hpToRecover = (int)(Random.Range(RecoveryPotency.x, RecoveryPotency.y) * MaxHP);
                RecoverHP(hpToRecover > 0 ? hpToRecover : 1);
            }
        }

        // Recover MP at all times
        lastMPRecovery += Time.deltaTime;

        if (CurrentHP > 0)
        {
            if (lastMPRecovery > MPRecoveryRate && CurrentMP < MaxMP)
            {
                lastMPRecovery = 0;
                int MPToRecover = (int)(Random.Range(MPRecoveryPotency.x, MPRecoveryPotency.y) * MaxMP);
                RecoverMP(MPToRecover);
            }
        }
    }

    protected float GetAttack()
    {
        return Random.Range(Attack.x, Attack.y) * (1 + (Mathf.Pow(CharacterLevel + AttackLevel - 2, 2) * AttackCoefficient));
    }

    protected float GetDefense()
    {
        return Random.Range(Defense.x, Defense.y) * (1 + (Mathf.Pow(CharacterLevel + DefenseLevel - 2, 2) * DefenseCoefficient));
    }

    public int TakeDamage(int damage, Character attacker = null)
    {
        if (CurrentHP > 0)
        {
            // Play sound on hit
            if (Audio != null && HitSound != null) Audio.PlayOneShot(HitSound);

            // Reset time out of battle
            lastHit = 0;

            // Calculate damage and display it
            int damageTaken = (int)(damage / GetDefense());
            damageTaken = damageTaken > 0 ? damageTaken : 1;

            DamageText.DisplayText(damageTaken.ToString(), transform, CompareTag("Player") ? ShowDamage.EDamageType.Player : ShowDamage.EDamageType.Monster);

            // If character has armor subtract from it first
            if (CurrentArmor > 0)
            {
                int diff = (int)CurrentArmor - damageTaken;
                if (diff > 0)
                {
                    CurrentArmor -= damageTaken;
                    damageTaken = 0;
                }
                else
                {
                    damageTaken -= (int)CurrentArmor;
                    CurrentArmor = 0;
                }

                UpdateArmorBar(false);
            }
            
            // Subtract from HP if any damage remains and character isn't immortal
            if (!Immortal) CurrentHP -= damageTaken;

            UpdateHPBar(false);

            // Death
            if (CurrentHP <= 0)
            {
                StopAllCoroutines();
                CurrentHP = 0;
                Die();
            }
            else StartCoroutine(GetHit(attacker));

            // Return damage taken in case the attack requires this value (lifesteal)
            return damageTaken;
        }
        return 0;
    }

    protected virtual IEnumerator GetHit(Character attacker)
    {
        yield return null;
    }

    public void RecoverHP(int recoveryAmount)
    {
        CurrentHP = Mathf.Clamp(CurrentHP + recoveryAmount, 0, MaxHP);
        if (recoveryAmount > 0) DamageText.DisplayText(recoveryAmount.ToString(), transform, ShowDamage.EDamageType.Heal);

        UpdateHPBar(false);
    }

    public void UpdateHPBar(bool instant)
    {
        if (HPBar != null)
        {
            HPBar.SetFill(CurrentHP / MaxHP, instant);
        }
    }

    public void RecoverMP(int recoveryAmount)
    {
        CurrentMP = Mathf.Clamp(CurrentMP + recoveryAmount, 0, MaxMP);
        if (recoveryAmount > 0) DamageText.DisplayText(recoveryAmount.ToString(), transform, ShowDamage.EDamageType.MP);

        UpdateMPBar(false);
    }

    public void UpdateMPBar(bool instant)
    {
        if (MPBar != null)
        {
            MPBar.SetFill(CurrentMP / MaxMP, instant);
        }
    }

    public void UpdateArmorBar(bool instant)
    {
        if (ArmorBar != null)
        {
            ArmorBar.SetFill(CurrentArmor / MaxHP, instant);
        }
    }

    public virtual void Die()
    {

    }
}
