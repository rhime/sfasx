﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArchingProjectile : MonoBehaviour
{
    [SerializeField] private float LaunchAngle;
    [SerializeField] private float SpeedMultiplier;
    [SerializeField] private float Lifetime;
    [SerializeField] private Explosion Explosion;
    [SerializeField] private float FloorLevel = 2.8f;

    private Rigidbody rigidbody;
    private Transform target;
    private Quaternion initialRotation;

    private Character attacker;
    private int Power;

    private float gravity;
    private float life;

    private bool lifeSteal;

    public void Setup(int power, Transform targetPos, Character source = null, bool stealLife = false)
    {
        Power = power;
        attacker = source;
        target = targetPos;
        life = Lifetime;

        initialRotation = transform.rotation;

        rigidbody = GetComponent<Rigidbody>();

        gravity = SpeedMultiplier * Physics.gravity.y;

        lifeSteal = stealLife;

        SetVelocity();        
    }

    public void Update()
    {
        life -= Time.deltaTime;
        if (life < 0)
        {
            Destroy(gameObject);
            return;
        }
        else if (transform.position.y < FloorLevel)
        {
            OnCollisionEnter(null);
        }
        if (rigidbody.velocity != Vector3.zero) transform.rotation = Quaternion.LookRotation(rigidbody.velocity) * initialRotation;
        rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y + gravity * Time.deltaTime, rigidbody.velocity.z); // Apply custom gravity force
    }

    private void SetVelocity()
    {
        float xzDistance = Vector3.Distance(new Vector3(transform.position.x, 0.0f, transform.position.z), new Vector3(target.position.x, 0.0f, target.position.z));
        float yDistance = target.position.y - transform.position.y;
        float tanAlpha = Mathf.Tan(LaunchAngle * Mathf.Deg2Rad);

        float zSpeed = Mathf.Sqrt(gravity * xzDistance * xzDistance / (2.0f * (yDistance - xzDistance * tanAlpha)));
        float ySpeed = tanAlpha * zSpeed;

        Vector3 localVelocity = new Vector3(0f, ySpeed, zSpeed);
        rigidbody.velocity = transform.TransformDirection(localVelocity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (Explosion != null)
        {
            Explosion explosion = GameObject.Instantiate(Explosion, new Vector3(transform.position.x, target.position.y, transform.position.z), Quaternion.identity);
            explosion.Explode(Power, attacker, lifeSteal);
            Destroy(gameObject);
        }
        else if (collision != null && collision.gameObject.CompareTag("Monster"))
        {
            collision.gameObject.GetComponent<Character>().TakeDamage(Power, attacker);
            Destroy(gameObject);
        }
        else Destroy(gameObject);
    }
}
