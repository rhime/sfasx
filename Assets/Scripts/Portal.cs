﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public bool isActive {get; set;}
    private bool teleporting = false;

    [SerializeField] private ParticleSystem[] ParticleSystems;

    [SerializeField] private AudioSource Audio;
    [SerializeField] private float SoundFadeSpeed = 0.25f;
    [SerializeField] private float StartVolume = 0.8f;

    [SerializeField] private GameObject Marker;

    public EnvironmentTile CurrentPosition;

    void Start()
    {
        Audio.volume = StartVolume;
        Initialise();
    }

    private void Update()
    {
        if (teleporting && !ParticleSystems[2].isPlaying) ParticleSystems[2].Play(true);
        else if (!teleporting && ParticleSystems[2].isPlaying)
        {
            ParticleSystems[2].Stop(true);
            StartCoroutine(SoundFadeOut.FadeOut(Audio, SoundFadeSpeed));
        }

        if (CurrentPosition != null) Marker.SetActive(CurrentPosition.Discovered);
    }

    public void Initialise()
    {
        isActive = false;
        teleporting = false;

        ParticleSystems[0].Pause(true);
        if (ParticleSystems[1].isPlaying) ParticleSystems[1].Stop();
    }

    public void Activate()
    {
        isActive = true;
        if (!ParticleSystems[1].isPlaying) ParticleSystems[1].Play();
    }

    public void ToggleStars(bool starsOn)
    {
        if (starsOn && isActive)
        {
            if (!teleporting)
            {
                StopAllCoroutines();
                Audio.Play();
                Audio.volume = StartVolume;
            }

            teleporting = true;
        }
        else teleporting = false;
    }
}
