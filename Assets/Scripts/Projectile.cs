﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float Speed;
    [SerializeField] private float Lifetime;
    private const float MinVelocity = 100;

    [SerializeField] private AudioSource Audio;

    private Rigidbody rigidbody;
    private Character source;
    private GameObject target;
    private int damage;
    private bool isUpgraded;

    private float life;
    private void Start()
    {
        life = Lifetime;
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = transform.forward * Speed;
    }

    private void Update()
    {
        life -= Time.deltaTime;
        if (life <= 0 || rigidbody.velocity.sqrMagnitude < MinVelocity) Destroy(gameObject);
        else if (isUpgraded && target != null)
        {
            transform.LookAt(target.transform);
            rigidbody.velocity = transform.forward * Speed;
        }
    }

    public void Setup(int power, Character source = null, GameObject target = null, bool upgraded = false)
    {
        damage = power;
        this.source = source;
        this.target = target;
        isUpgraded = upgraded;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (Audio != null) Audio.PlayOneShot(Audio.clip);
        if (collision.gameObject.CompareTag("Monster"))
        {
            collision.gameObject.GetComponent<Character>().TakeDamage(damage, source);
            Destroy(gameObject);
        }

        else if (!isUpgraded) life -= Lifetime / 3; // Base projectiles die after 2 bounces at most
        else if (collision.gameObject.CompareTag("Door") && !collision.gameObject.GetComponentInParent<Door>().IsOpen)
        {
            // Prevent occassional passing through closed doors
            rigidbody.velocity /= 2;
            life -= Lifetime / 3;

        }
        else life -= Lifetime / 9; // Upgraded projectiles can bounce more
    }
}
