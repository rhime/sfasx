﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectVariation : MonoBehaviour
{
    [SerializeField] private List<GameObject> OptionalItems;
    [SerializeField] private float RemovalChance;
    [SerializeField] private Vector2 RotationRange;
    [SerializeField] private Vector2 ScaleRange = new Vector2(1, 1);

    void Start()
    {
        transform.Rotate(new Vector3(0, Random.Range(RotationRange.x, RotationRange.y), 0));

        float scale = Random.Range(ScaleRange.x, ScaleRange.y);
        transform.localScale = new Vector3(scale, scale, scale);

        for (int i = 0; i < OptionalItems.Count; i++)
        {
            if (Random.Range(0.0f, 1.0f) < RemovalChance) Destroy(OptionalItems[i]);
        }
    }
}
