﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class Save
{
    public int currentFloor;
    public int playerLevel;
    public int playerAttackLevel;
    public int playerDefenseLevel;
    public int playerExp;

    public Environment.STile[,] map;
    public List<Environment.SRoom> rooms;

    public TaskController.ETaskType taskType;
    public int requirement;

    public void SaveGame(string savePath)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(savePath);
        bf.Serialize(file, this);
        file.Close();
    }
    public Save LoadGame(string savePath)
    {
        if (File.Exists(savePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(savePath, FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();

            return save;
        }
        else return null;   
    }
}
