﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowDamage : MonoBehaviour
{
    [SerializeField] private GameObject DamageText;
    [SerializeField] private Color MonsterDamageColor;
    [SerializeField] private Color PlayerDamageColor;
    [SerializeField] private Color HealColor;
    [SerializeField] private Color MPColor;
    [SerializeField] private Color ExpColor;
    [SerializeField] private Canvas HUD;

    public enum EDamageType
    {
        Monster,
        Player,
        Heal,
        MP,
        Exp
    }

    public void DisplayText(string text, Transform source, EDamageType damageType)
    {
        GameObject damageText = Instantiate(DamageText, transform);

        damageText.GetComponentInChildren<TextMeshProUGUI>().text = text;

        switch (damageType)
        {
            case EDamageType.Monster:
            {
                damageText.GetComponentInChildren<TextMeshProUGUI>().color = MonsterDamageColor;
                break;
            }
            case EDamageType.Player:
            {
                damageText.GetComponentInChildren<TextMeshProUGUI>().color = PlayerDamageColor;
                break;
            }
            case EDamageType.Heal:
            {
                damageText.GetComponentInChildren<TextMeshProUGUI>().color = HealColor;
                break;
            }
            case EDamageType.Exp:
            {
                damageText.GetComponentInChildren<TextMeshProUGUI>().color = ExpColor;
                break;
            }
            case EDamageType.MP:
            {
                damageText.GetComponentInChildren<TextMeshProUGUI>().color = MPColor;
                break;
            }
        }
        damageText.GetComponent<UIPopUpText>().SetSource(source, HUD);
    }
}
