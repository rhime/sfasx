﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public EnvironmentTile Tile;

    [SerializeField] private GameObject DoorParent;
    [SerializeField] private float Speed;
    [SerializeField] private float OpenRatio;

    [SerializeField] public float ToggleDistance;

    [SerializeField] private AudioSource Audio;

    public bool IsOpen;

    Quaternion open = Quaternion.AngleAxis(90, Vector3.up);
    Quaternion closed;

    Quaternion targetRotation;

    public void SetupDoor(EnvironmentTile tile)
    {
        Tile = tile;

        closed = DoorParent.transform.localRotation;
        open = Quaternion.AngleAxis(90, DoorParent.transform.up);

        IsOpen = OpenRatio > Random.value;
        Toggle(false);

        DoorParent.transform.rotation = targetRotation;
    }

    private void Update()
    {
        if (!IsOpen) Tile.IsAccessible = false; // In case something walked in at wrong time

        if (DoorParent.transform.localRotation != targetRotation)
        {
            DoorParent.transform.localRotation = Quaternion.RotateTowards(DoorParent.transform.localRotation, targetRotation, Speed * Time.deltaTime);
        }
    }

    public void Toggle(bool playSound = true)
    {
        if (!(IsOpen && !Tile.IsAccessible)) // If the door is open make sure nothing is occupying the space
        {
            if (playSound) Audio.PlayOneShot(Audio.clip);

            IsOpen = !IsOpen;
            Tile.IsAccessible = IsOpen;
            targetRotation = IsOpen ? open : closed;
        }
    }
}
