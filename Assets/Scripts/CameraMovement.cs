﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private const float SmoothTime = 0.05f;
    private const float SmoothLimit = 350.0f;

    [SerializeField] private GameObject Player;

    private Vector3 defaultPosition;

    void Start()
    {
        defaultPosition = transform.position;
        ResetPosition();
    }

    public void ResetPosition()
    {
        transform.position = defaultPosition + Player.transform.position;
    }

    void LateUpdate()
    {
        Vector3 velocity = Vector3.zero;
        transform.position = Vector3.SmoothDamp(transform.position, defaultPosition + Player.transform.position, ref velocity, SmoothTime, SmoothLimit);
    }
}
