﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCamera : MonoBehaviour
{
    [SerializeField] private GameObject target;
    private Vector3 startPos;
    [SerializeField] private Vector2 ZoomBounds = new Vector2(10, 140);
    private Camera camera;

    private void Start()
    {
        camera = GetComponent<Camera>();

        if (!PlayerPrefs.HasKey("MapSize")) PlayerPrefs.SetFloat("MapSize", camera.orthographicSize);
        else camera.orthographicSize = PlayerPrefs.GetFloat("MapSize", camera.orthographicSize);

        startPos = transform.position;
    }
    void LateUpdate()
    {
        transform.position = startPos + target.transform.position;
    }

    public void Zoom(float zoomSize)
    {
        camera.orthographicSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize + zoomSize, ZoomBounds.x, ZoomBounds.y);
        PlayerPrefs.SetFloat("MapSize", camera.orthographicSize);
    }
}
