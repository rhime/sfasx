﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Character
{
    public enum EMonsterMove
    {
        Tackle = 0,
        Charge = 1,
        Pounce = 2
    }

    // Loot and exp
    [SerializeField] private List<GameObject> Loot = new List<GameObject>();
    [SerializeField] private float BaseExp = 1.5f;
    private const float ExpGrowth = 1.3f;
    private int expGiven;

    // Timing
    private Vector2 PatrolPause = new Vector2 (0.2f, 3); // Pause between moving to another patrol location
    private float HitCooldown = 0.2f;
    private float timeSinceHit = 0;
    private float FlinchTime = 0.02f;

    // Movement
    private Vector2 SpeedMultiplierRange = new Vector2(0.8f, 1.2f);

    // Moves
    [SerializeField] private List<EMonsterMove> Moveset; // Moves the monster can use

    private List<int> MoveWeight = new List<int> { 6, 2, 3 }; // How likely an attack is to be used
    private List<Vector2> MoveConstraint = new List<Vector2> { new Vector2(0, 1), new Vector2(0.1f, 0.9f), new Vector2(0, 0.8f) }; // Health ratio required to execute attack
    [SerializeField] private List<float> MoveAnimationSpeed = new List<float> { 1, 1, 1 }; // Animation speed when executing attacks
    [SerializeField] private List<string> MoveAnimationTrigger = new List<string> { "Attack1", "Attack2", "Attack2" };
    private List<Vector2> MovePause = new List<Vector2> { new Vector2(0.5f, 2), new Vector2(0.4f, 0.5f), new Vector2(1, 2.5f) }; // Pause after attack
    private List<float> MovePower = new List<float> { 1, 3, 2.5f }; // Power multiplier of each attack
    private Vector2 ChargeStunTime = new Vector2(2, 3.5f);

    // Animation
    [SerializeField] private float AnimatorHitSpeed = 1.5f;
    [SerializeField] private float AnimatorDieSpeed = 1.0f;
    [SerializeField] private float AnimatorLookSpeed = 1.0f;
    [SerializeField] private float AnimatorAlertSpeed = 1.0f;

    [SerializeField] private float AnimatorStunSpeed = 1.0f;

    private bool isPaused = false;

    // Ranges
    [SerializeField] private GameObject ChaseRange;
    [SerializeField] private GameObject SightRange;

    private float attackRange = 225;
    
    // Tile map
    public Environment mMap;

    // Sound
    [SerializeField] protected AudioClip DeathSound;
    [SerializeField] protected AudioClip AlertSound;
    [SerializeField] protected List<AudioClip> MoveSound;

    // State
    public enum EMonsterState
    {
        Inactive,
        Patrol,
        Alert,
        Dead
    }
    public EMonsterState monsterState = EMonsterState.Inactive;
    public Environment.SRoom monsterRoom;
    private EnvironmentTile nextPosition;

    // Map
    [SerializeField] private GameObject Marker;

    // Setup
    public void MonsterSetup(int floorNumber, EnvironmentTile tile, Environment.SRoom room)
    {
        // Update stats
        CharacterLevel = floorNumber;
        MaxHP = Random.Range(MaxHPRange.x, MaxHPRange.y) * (1 + Mathf.Pow(floorNumber, 2) * HPCoefficient);
        CurrentHP = MaxHP;

        // Vary the speed a little
        SpeedMultiplier = Random.Range(SpeedMultiplierRange.x, SpeedMultiplierRange.y);

        // Initial position and rotation
        transform.position = tile.Position;
        CurrentPosition = tile;
        transform.Rotate(new Vector3(0, Random.Range(0, 360), 0));

        // Used for pathfinding when patrolling and chasing the player
        mMap = tile.GetComponentInParent<Environment>();
        monsterRoom = room;

        // Set eperience given on death
        expGiven = (int)(BaseExp * Mathf.Pow(floorNumber, ExpGrowth));
    }

    // Updates
    protected override void CustomUpdate()
    {
        // Map marker is visible only if the tile has been discovered
        Marker.SetActive(CurrentPosition.Discovered);

        // Counts time since last incoming or outgoing attack to determine if the monster is out of battle
        timeSinceHit += Time.deltaTime;
    }

    // Animation
    public override void Idle()
    {
        isRunning = false;

        if (movementState != EMovement.Idle)
        {
            movementState = EMovement.Idle;
            if (monsterState != EMonsterState.Alert)
            {
                animator.SetTrigger("Idle");
            }
            else
            {
                animator.SetTrigger("IdleBattle");
            }
        }
        animator.speed = AnimatorIdleSpeed;
    }

    // Battle and states
    private void GrantExp()
    {
        GameObject.FindWithTag("Player").GetComponent<Player>().GetExp(expGiven);
    }

    protected override IEnumerator GetHit(Character attacker = null)
    {
        if (timeSinceHit > HitCooldown) 
        {
            timeSinceHit = 0; // Monster is in battle and can't heal for a while
            isPaused = true; // Prevents monster from attacking while animation plays

            if (nextPosition != null && nextPosition != CurrentPosition) nextPosition.IsAccessible = true; //If the monster didn't reach next tile, free it

            animator.SetTrigger("GetHit"); // Play animation
            animator.speed = AnimatorHitSpeed;

            yield return new WaitForSeconds(FlinchTime);

            isPaused = false;
            if (attacker != null)
            {
                GoAlert(attacker, true); // Chase/attack the player
            }

            yield return null;
        }
    }

    public override void Die()
    {
        // Death sound
        if (Audio != null && DeathSound != null) Audio.PlayOneShot(DeathSound);

        // Play dying animation
        monsterState = EMonsterState.Dead;
        animator.SetTrigger("Die");
        animator.SetBool("Alive", false);
        animator.speed = AnimatorDieSpeed;

        // If the current task is to kill monsters, update progress
        TaskController task = FindObjectOfType<TaskController>();
        if (task.type == TaskController.ETaskType.MonsterKill)
        {
            if (task.IncreaseProgress())
            {
                FindObjectOfType<Portal>().Activate();
            }
        }

        // Drop a random item from the list
        if (Loot.Count > 0)
        {
            GameObject loot = Instantiate(Loot[Random.Range(0, Loot.Count)], transform.position, transform.rotation);
            loot.transform.parent = FindObjectOfType<EnvironmentTile>().transform; // Attach drop to a tile so that it can be destroyed along with it when the floor is reloaded
        }

        GrantExp();

        // Destroy colliders so the monster doesn't block the tile while dying
        Collider[] colliders = gameObject.GetComponentsInChildren<Collider>();
        foreach (Collider c in colliders) Destroy(c, 0.45f);

        // Free the occupied tile(s)
        CurrentPosition.IsAccessible = true;
        if (nextPosition != null) nextPosition.IsAccessible = true;

        // Destroy monster after the animation/sound finishes
        float deathTime = DeathSound != null ? DeathSound.length : animator.GetCurrentAnimatorStateInfo(0).length;
        Destroy(gameObject, deathTime);
    }

    public void StartPatrol()
    {
        if (monsterState != EMonsterState.Patrol)
        {
            ChaseRange.SetActive(false);
            SightRange.SetActive(true);

            if (nextPosition != null && nextPosition != CurrentPosition) nextPosition.IsAccessible = true;
            StopAllCoroutines();
            StartCoroutine(Patrol());
        }
    }

    public void GoAlert(Character target, bool onAttack = false)
    {
        if (monsterState == EMonsterState.Patrol)
        {
            if (!onAttack && Audio != null && AlertSound != null) Audio.PlayOneShot(AlertSound);

            ChaseRange.SetActive(true);
            SightRange.SetActive(false);

            if (nextPosition != null && nextPosition != CurrentPosition) nextPosition.IsAccessible = true;
            StopAllCoroutines();
            StartCoroutine(Alert(target, onAttack));
        }
    }

    IEnumerator TurnToward(Vector3 targetPosition, float multiplier = 1.0f)
    {
        Quaternion targetRotation = Quaternion.LookRotation(targetPosition - transform.position);
        while (transform.rotation != targetRotation)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 5.0f);
            yield return new WaitForSeconds(0.005f / multiplier);
        }

        yield return null;
    }

    IEnumerator Patrol()
    {
        isRunning = false;

        monsterState = EMonsterState.Patrol;

        List<EnvironmentTile> route;
        EnvironmentTile patrolPoint = null;

        while (true)
        {
            // Pause movement and look around
            CurrentPosition.IsAccessible = false;
            Idle();
            yield return new WaitForSeconds(Random.Range(PatrolPause.x, PatrolPause.y));

            animator.SetTrigger("LookAround");
            animator.speed = AnimatorLookSpeed;
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length / AnimatorLookSpeed);

            if (CurrentPosition == null)
            {
                CurrentPosition = GameObject.FindGameObjectWithTag("Floor").GetComponent<EnvironmentTile>();
            }

            // Pick a tile to go to and get a path leading to it
            route = mMap.GetPatrolRoute(CurrentPosition, ref patrolPoint, monsterRoom);
            int routeIndex = 1;

            Walk();

            while (route != null && route.Count > routeIndex)
            {
                // Turn towards the next point in path
                yield return TurnToward(route[routeIndex].Position, 2.0f);

                // Make sure the path is still free, if it's not try to get a new one
                if (!route[routeIndex].IsAccessible)
                {
                    route = mMap.Solve(CurrentPosition, patrolPoint);
                    if (route == null) route = mMap.GoNearTarget(CurrentPosition, patrolPoint);
                    routeIndex = 1;
                    break;
                }

                //If path is free or getting a new path was succesful, move toward the next point in path
                if (route != null && route.Count > 1)
                {
                    nextPosition = route[routeIndex];
                    route[routeIndex].IsAccessible = false;
                    stepTimer = 100; // Reset the step sound timer so a sound is made right away
                    yield return DoMove(transform.position, route[routeIndex].Position);

                    routeIndex++;
                }
            }

            // If there is no path to follow wait a bit before trying to find one again
            if (route == null) yield return new WaitForSeconds(1.0f);
        }
    }

    IEnumerator Alert(Character target, bool onAttack = false)
    {
        EnvironmentTile targetTile = target.CurrentPosition;
        monsterState = EMonsterState.Alert;

        // Look at target
        yield return TurnToward(target.transform.position, 3.0f);

        if (!onAttack)
        {
            // If the player was just noticed make a sound
            animator.SetTrigger("Alert");
            animator.speed = AnimatorAlertSpeed;
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length / AnimatorAlertSpeed);
        }

        while (true)
        {
            yield return new WaitUntil(() => !isPaused);
            lastHit = 0;

            float offset = (transform.position - target.transform.position).sqrMagnitude;
            if (offset < attackRange && offset > 30) // Minimum distance added in case the monster walks into the player
            {
                // Look at target and attack
                Idle();
                CurrentPosition.IsAccessible = false;
                animator.SetTrigger("IdleBattle");
                yield return TurnToward(target.transform.position, 3.0f);
                yield return MonsterMove(SelectMove());

            }
            else
            {
                // If target is too far try to get closer
                List<EnvironmentTile> route = mMap.GoNearTarget(CurrentPosition, target.CurrentPosition);
                int routeIndex = 1;

                while (route != null && route.Count > routeIndex)
                {
                    Idle();
                    CurrentPosition.IsAccessible = false;
                    yield return TurnToward(route[routeIndex].Position, 2.0f);

                    bool newRoute = targetTile != target.CurrentPosition; // If target moved the route needs to be adjusted
                    if (!newRoute)
                    {
                        for (int i = routeIndex; i < route.Count; i++)
                            if (!route[i].IsAccessible) // It also needs to be adjusted if any of the tiles in the path were occupied
                            {
                                newRoute = true;
                                break;
                            }
                    }
                    if (newRoute)
                    {
                        route = mMap.GoNearTarget(CurrentPosition, target.CurrentPosition);
                        routeIndex = 1;
                    }

                    if (route != null)
                    {
                        // Move onto the next point in path 
                        nextPosition = route[routeIndex];
                        route[routeIndex].IsAccessible = false;
                        Run();
                        stepTimer = 100;
                        yield return DoMove(transform.position, route[routeIndex].Position);

                        routeIndex++;
                    }
                }

                if (route == null)
                {
                    // Wait if a path to the target couldn't be found
                    yield return TurnToward(target.transform.position);
                    Idle();
                    yield return new WaitForSeconds(1);
                }
            }
        }
    }

    EMonsterMove SelectMove()
    {
        // List the moves that can be used
        List<EMonsterMove> possibleMoves = new List<EMonsterMove>();
        List<int> weights = new List<int>();
        for (int i = 0; i < Moveset.Count; i++)
        {
            int moveNo = (int)Moveset[i];
            if (CurrentHP / MaxHP >= MoveConstraint[moveNo].x && CurrentHP / MaxHP <= MoveConstraint[moveNo].y)
            {
                possibleMoves.Add(Moveset[i]);
                weights.Add(MoveWeight[moveNo]);
            }
        }

        // Return basic attack if nothing can be executed
        if (possibleMoves.Count == 0) return EMonsterMove.Tackle;

        // Otherwise select a random move based on weigth
        int totalWeigth = 0;
        for (int i = 0; i < weights.Count; i++) totalWeigth += weights[i];

        int result = Random.Range(0, totalWeigth);
        int index = 0;

        while (result >= weights[index])
        {
            result -= weights[index];
            index++;
        }
        return possibleMoves[index];
    }
    IEnumerator MonsterMove(EMonsterMove move)
    {
        int moveNo = (int)move;

        // Show move animation and play sound
        animator.SetTrigger(MoveAnimationTrigger[moveNo]);
        animator.speed = MoveAnimationSpeed[moveNo];

        if (Audio != null && MoveSound[(int)move] != null) Audio.PlayOneShot(MoveSound[(int)move]);

        float moveTime = animator.GetCurrentAnimatorStateInfo(0).length / MoveAnimationSpeed[moveNo];
        yield return new WaitForSeconds(moveTime * 0.4f);

        // Calculate move power and use it
        int attackPower = (int)(GetAttack() * MovePower[moveNo]);

        switch (move)
        {
            case EMonsterMove.Tackle:
            {
                AttackSpawn.Tackle(attackPower);
                break;
            }
            case EMonsterMove.Charge:
            {
                AttackSpawn.Charge(attackPower);
                break;
            }
            case EMonsterMove.Pounce:
            {
                AttackSpawn.Pounce(attackPower);
                break;
            }
        }

        // Wait for the animation to finish
        yield return new WaitForSeconds(moveTime * 0.6f);

        // Stun the monster if it used charge
        if (move == EMonsterMove.Charge)
        {
            animator.SetTrigger("Stun");
            animator.speed = AnimatorStunSpeed;
            yield return new WaitForSeconds(Random.Range(ChargeStunTime.x, ChargeStunTime.y));
        }

        // Set animation to idle and wait a little before attacking again or following the target
        animator.SetTrigger("IdleBattle");
        animator.speed = AnimatorIdleSpeed;
        yield return new WaitForSeconds(Random.Range(MovePause[moveNo].x, MovePause[moveNo].y));
    }
}
