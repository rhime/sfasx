﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageFade : MonoBehaviour
{
    [SerializeField] private UnityEngine.UI.Image Image;
    [SerializeField] private float FadeSpeed;
    [SerializeField] private float FadeValue;
    private void Start()
    {
        SetFade(FadeValue);
    }

    enum EFadeStatus
    {
        Paused,
        FadeIn,
        FadeOut
    }

    EFadeStatus fadeStatus = EFadeStatus.Paused;

    private void Update()
    {
        switch (fadeStatus)
        {
            case (EFadeStatus.FadeIn):
                {
                    FadeValue += FadeSpeed * Time.deltaTime;
                    if (FadeValue > 1.0f)
                    {
                        FadeValue = 1.0f;
                        fadeStatus = EFadeStatus.Paused;
                    }
                    break;
                }

            case (EFadeStatus.FadeOut):
                {
                    FadeValue -= FadeSpeed * Time.deltaTime;
                    if (FadeValue < 0.0f)
                    {
                        FadeValue = 0.0f;
                        fadeStatus = EFadeStatus.Paused;
                    }
                    break;
                }
        }

        SetFade(FadeValue);
    }

    public void FadeIn()
    {
        fadeStatus = EFadeStatus.FadeIn;
    }

    public void FadeOut()
    {
        fadeStatus = EFadeStatus.FadeOut;
    }

    public bool IsPaused()
    {
        return fadeStatus == EFadeStatus.Paused;
    }

    void SetFade(float value)
    {
        FadeValue = value;
        var newColour = Image.color;
        newColour.a = value;
        Image.color = newColour;
    }
}
