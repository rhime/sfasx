﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] private float Radius;
    [SerializeField] private Vector2 LifeStealPotency;
    public void Explode(int power, Character source = null, bool lifeSteal = false)
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, Radius);
        for (int i = 0;  i < hitColliders.Length; i++)
        {
            if (hitColliders[i].CompareTag("Monster"))
            {
                Monster monsterHit = hitColliders[i].gameObject.GetComponent<Monster>();

                int damageTaken = monsterHit.TakeDamage(power, source);

                if (lifeSteal) source.RecoverHP((int)(damageTaken * Random.Range(LifeStealPotency.x, LifeStealPotency.y)));
            }
        }
    }
}
