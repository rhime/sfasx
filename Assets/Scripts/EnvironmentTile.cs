﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentTile : MonoBehaviour
{
    public List<EnvironmentTile> Connections { get; set; }
    public EnvironmentTile Parent { get; set; }
    [SerializeField] public Vector3 Position { get; set; }
    public float Global { get; set; }
    public float Local { get; set; }
    public bool Visited { get; set; }
    public bool IsAccessible { get; set; }
    public bool MakeAccessible { get; set; }
    public bool Discovered { get; set; }

    [SerializeField] private GameObject mapQuad;
    private GameObject player;
    private float discoveryDist = 2500;
    public bool HasObject { get; set; }

    public void MapSetup()
    {
        // If the tile is not a wall, start checking distance from player and reveal itself when they're near
        player = GameObject.FindGameObjectWithTag("Player");
        if (mapQuad != null && !Discovered) StartCoroutine(Reveal());
    }

    private IEnumerator Reveal()
    {
        while (Connections == null) yield return null;

        while (!Discovered)
        {
            Vector3 offset = Position - player.transform.position;
            if (offset.sqrMagnitude < discoveryDist)
            {
                Discovered = true;
                mapQuad.SetActive(true);

                TaskController task = FindObjectOfType<TaskController>();
                if (task.type == TaskController.ETaskType.ChartMap)
                {
                    if (task.IncreaseProgress())
                    {
                        FindObjectOfType<Portal>().Activate();
                    }
                }

                Color color = mapQuad.GetComponent<Renderer>().material.color;
                color.a = HasObject ? 0.95f : 1.0f;
                mapQuad.GetComponent<Renderer>().material.color = color;

                for (int i = 0; i < Connections.Count; i++)
                {
                    if (!Connections[i].Discovered && Connections[i].mapQuad != null)
                    {
                        Connections[i].mapQuad.SetActive(true);

                        for (int j = 0; j < Connections[i].Connections.Count; j++)
                        {
                            if (!Connections[i].Connections[j].Discovered && Connections[i].Connections[j].mapQuad != null) Connections[i].Connections[j].mapQuad.SetActive(true);
                        }
                    }
                }
            }
            yield return new WaitForSeconds(0.4f);
        }
        yield return null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (MakeAccessible)
        {
            if (CompareTag("Floor") && other.CompareTag("TileCollider"))
            {
                IsAccessible = false;
                if (other.GetComponentInParent<Character>().CurrentPosition != this)
                {
                    other.GetComponentInParent<Character>().CurrentPosition.IsAccessible = true;
                    other.GetComponentInParent<Character>().CurrentPosition = this;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (MakeAccessible)
        {
            if (CompareTag("Floor") && other.CompareTag("TileCollider")) IsAccessible = true;
        }
    }
}
