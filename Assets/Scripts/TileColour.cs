﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileColour : MonoBehaviour
{
    [SerializeField] private List<Color> Colors;
    [SerializeField] private string matName;

    void Start()
    {
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
        {
            foreach (Material m in r.materials)
            {
                if (m.name == matName + " (Instance)") m.color = Colors[Random.Range(0, Colors.Count)];
            }
        }
    }
}
