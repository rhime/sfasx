﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TaskController : MonoBehaviour
{
    [SerializeField] private Vector2Int[] RequirementNumber;
    [SerializeField] private TextMeshProUGUI TaskText;
    
    [SerializeField] private AudioSource Audio;
    [SerializeField] private AudioClip SoulCollectSound;

    [System.Serializable]
    public enum ETaskType
    {
        SoulCollect = 0,
        MonsterKill = 1,
        ChartMap = 2
    }

    public ETaskType type { get; set; }

    public int progress { get; set; }
    public int requirement { get; set; }

    string taskText;

    public ETaskType NewTask(ref int reqNumber)
    {
        type = (ETaskType)Random.Range(0, RequirementNumber.Length);

        progress = 0;
        requirement = Random.Range(RequirementNumber[(int)type].x, RequirementNumber[(int)type].y + 1);

        SetProgressText();

        reqNumber = requirement;
        return type;
    }

    public void LoadTask(ETaskType taskType, int required)
    {
        type = taskType;
        requirement = required;

        progress = 0;

        SetProgressText();
    }

    private void UpdateText()
    {
        TaskText.text = taskText;
    }

    private void SetProgressText()
    {
        string progressText = '[' + progress.ToString() + '/' + requirement.ToString() + ']';

        switch (type)
        {
            case ETaskType.SoulCollect:
                {
                    taskText = "Collect " + requirement.ToString() + " souls to proceed. " + progressText;
                    break;
                }
            case ETaskType.MonsterKill:
            {
                taskText = "Slay " + requirement.ToString() + " monsters to proceed. " + progressText;
                break;
            }
            case ETaskType.ChartMap:
            {
                taskText = "Chart the floor's map. " + progressText;
                break;
            }
        }
        UpdateText();
    }

    private void SetCompleteText()
    {
        taskText = "A passage has opened nearby...";
        UpdateText();
    }

    public bool IncreaseProgress()
    {
        if (progress < requirement)
        {
            if (type == ETaskType.SoulCollect && Audio != null && SoulCollectSound != null) Audio.PlayOneShot(SoulCollectSound);

            progress++;
            SetProgressText();
            if (progress == requirement)
            {
                SetCompleteText();
                return true;
            }
        }
        return false;
    }
}

