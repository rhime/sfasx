﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    private Character attacker;
    private int Power;
    
    void Start()
    {
        Destroy(gameObject, 0.5f);
    }

    public void Setup(int power, Character source = null)
    {
        Power = power;
        attacker = source;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Character>().TakeDamage(Power, attacker);
            Destroy(gameObject);
        }
    }
}
