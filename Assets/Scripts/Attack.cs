﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    [SerializeField] private Character Source;

    [SerializeField] private Projectile MagicMissileProjectile;
    [SerializeField] private ArchingProjectile FireballProjectile;
    [SerializeField] private ArchingProjectile FireballAltProjectile;
    [SerializeField] private ArchingProjectile VoidProjectile;

    [SerializeField] private MeleeAttack TackleCollider;
    [SerializeField] private MeleeAttack ChargeCollider;
    [SerializeField] private MeleeAttack PounceCollider;

    public void MagicMissile(int damage, GameObject target = null, bool upgraded = false)
    {
        if (MagicMissileProjectile != null)
        {
            Projectile newProjectile = Instantiate(MagicMissileProjectile, transform.position, transform.rotation);
            newProjectile.Setup(damage, Source, target, upgraded);
        }
    }

    public void Fireball(int damage, Transform target, bool upgraded = false)
    {
        if (FireballProjectile != null)
        {
            ArchingProjectile newProjectile = Instantiate(upgraded ? FireballAltProjectile : FireballProjectile, transform.position, transform.rotation);
            newProjectile.Setup(damage, target, Source);
        }
    }

    public void Void(int damage, Transform target, bool upgraded = false)
    {
        if (VoidProjectile != null)
        {
            ArchingProjectile newProjectile = Instantiate(VoidProjectile, transform.position, transform.rotation);
            newProjectile.Setup(damage, target, Source, upgraded);
        }
    }

    public void Tackle(int damage)
    {
        if (TackleCollider != null)
        {
            MeleeAttack attack = Instantiate(TackleCollider, transform.position, transform.rotation);
            attack.Setup(damage, Source);
        }
    }

    public void Charge(int damage)
    {
        if (ChargeCollider != null)
        {
            MeleeAttack attack = Instantiate(ChargeCollider, transform.position, transform.rotation);
            attack.Setup(damage, Source);
        }
    }

    public void Pounce(int damage)
    {
        if (PounceCollider != null)
        {
            MeleeAttack attack = Instantiate(PounceCollider, transform.position, transform.rotation);
            attack.Setup(damage, Source);
        }
    }
}
