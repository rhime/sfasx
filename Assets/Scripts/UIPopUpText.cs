﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopUpText : MonoBehaviour
{
    private Transform location;
    private Vector2 screenSize;

    public void SetSource(Transform source, Canvas canvas)
    {
        location = source;
        screenSize.x = canvas.GetComponent<RectTransform>().rect.width;
        screenSize.y = canvas.GetComponent<RectTransform>().rect.height;
        Update();
    }

    private void Update()
    {
        if (location != null)
        {
            // Updated every frame so it doesn't move with the player
            Vector2 screenPosition = Camera.main.WorldToViewportPoint(location.position);
            transform.localPosition = new Vector2(screenSize.x * (screenPosition.x - 0.5f), screenSize.y * (screenPosition.y - 0.5f));
        }
    }
}
