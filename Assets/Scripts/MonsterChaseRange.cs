﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterChaseRange : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (GetComponentInParent<Monster>().monsterState == Monster.EMonsterState.Alert && other.tag == "Player")
        {
            GetComponentInParent<Monster>().StartPatrol();
        }
    }
}
