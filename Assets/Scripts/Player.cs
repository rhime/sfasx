﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : Character
{
    // UI
    [SerializeField] private float ExpMultiplier = 1;
    [SerializeField] private UISetBarFill ExpBar;
    [SerializeField] private TextMeshProUGUI LevelText;
    [SerializeField] private TextMeshProUGUI AttackLevelText;
    [SerializeField] private TextMeshProUGUI DefenseLevelText;

    // Experience
    public int CurrentExp = 0;
    private int ExpRequired = 1;

    // Skills
    private float MagicMissileFireRate = 0.4f;
    private float FireballFireRate = 0.5f;
    private float VoidFireRate = 0.6f;
    private float lastShot = 0;

    private const float MagicMissilePower = 0.95f;
    private const float MagicMissileUpgradedPower = 1;
    private const float FireballPower = 2.2f;
    private const float FireballUpgradedPower = 3.1f;
    private const float VoidPower = 3.5f;
    private const float VoidUpgradedPower = 4;
    private const float ShieldPotency = 0.3f;
    private const float ShieldUpgradedPotency = 0.5f;

    // Sound
    [SerializeField] private AudioClip ShieldSound;

    [SerializeField] private AudioClip LevelUpSound;
    [SerializeField] private AudioClip StatUpSound;

    protected override void CustomUpdate()
    {
        // Let player use skills again if enough time passed since cast
        lastShot -= Time.deltaTime;
    }

    public void AttackUp(int num)
    {
        if (num > 0 && Audio != null && StatUpSound != null) Audio.PlayOneShot(StatUpSound);

        AttackLevel += num;
        AttackLevelText.text = AttackLevel.ToString();
    }

    public void DefenseUp(int num)
    {
        if (num > 0 && Audio != null && StatUpSound != null) Audio.PlayOneShot(StatUpSound);

        DefenseLevel += num;
        DefenseLevelText.text = DefenseLevel.ToString();

        float newMaxHealth = (int)(MaxHPRange.x * (1 + (Mathf.Pow((DefenseLevel + CharacterLevel) * 0.6f - 1, 2) * HPCoefficient)));
        CurrentHP = Mathf.Clamp(CurrentHP + newMaxHealth - MaxHP, 0, MaxHP);
        MaxHP = newMaxHealth;
    }

    public void GetExp(int amount)
    {
        CurrentExp += (int)(amount * ExpMultiplier);
        if (amount > 0) DamageText.DisplayText(amount.ToString(), transform, ShowDamage.EDamageType.Exp);
        FindObjectOfType<Game>().UpdatePlayerExp();
    }

    public IEnumerator UpdateExp()
    {
        // Level up
        while (CurrentExp >= ExpRequired)
        {
            ExpBar.SetFill(1.5f);
            yield return new WaitUntil(() => ExpBar.currentFill >= 1.0f);

            CharacterLevel++;
            DefenseUp(0);
            if (Audio != null && LevelUpSound != null) Audio.PlayOneShot(LevelUpSound);
            FindObjectOfType<Game>().SkillSetup();
            UpdateLevelText();

            CurrentExp -= ExpRequired;
            SetRequiredExp();

            ExpBar.SetFill(0, true);
        }
        // Update exp bar
        ExpBar.SetFill((float)CurrentExp / ExpRequired);
        yield return null;
    }

    public void UpdateLevelText()
    {
        LevelText.text = CharacterLevel.ToString();
    }

    public void SetRequiredExp()
    {
        ExpRequired = Mathf.RoundToInt(3 * Mathf.Pow(CharacterLevel + 1, 3) / 4);
    }

    public void CastSkill(Game.EPlayerSkill skillType, float MP, GameObject target = null, bool upgraded = false)
    {
        if (lastShot <= 0 || skillType == Game.EPlayerSkill.Shield)
        {
            CurrentMP -= MP;

            switch (skillType)
            {
                case Game.EPlayerSkill.MagicMissile:
                {
                    MagicMissile(target, upgraded);
                    break;
                }
                case Game.EPlayerSkill.Fireball:
                {
                    Fireball(target, upgraded);
                    break;
                }
                case Game.EPlayerSkill.Void:
                {
                    Void(target, upgraded);
                    break;
                }
                case Game.EPlayerSkill.Shield:
                {
                    Shield(upgraded);
                    break;
                }
            }

            UpdateMPBar(false);
        }
    }

    private void MagicMissile(GameObject target, bool upgraded = false)
    {
        if (AttackSpawn != null && target != null && lastShot <= 0)
        {
            StopAllCoroutines();
            Idle();

            transform.LookAt(target.transform);

            AttackSpawn.MagicMissile((int)(GetAttack() * (upgraded ? MagicMissileUpgradedPower : MagicMissilePower)), target, upgraded);

            lastShot = MagicMissileFireRate;
            lastHit = 0;
        }
    }

    private void Fireball(GameObject target, bool upgraded = false)
    {
        if (AttackSpawn != null && target != null && lastShot <= 0)
        {
            StopAllCoroutines();
            Idle();

            transform.LookAt(target.transform);

            AttackSpawn.Fireball((int)(GetAttack() * (upgraded ? FireballUpgradedPower : FireballPower)), target.transform, upgraded);

            lastShot = FireballFireRate;
            lastHit = 0;
        }
    }

    private void Void(GameObject target, bool upgraded = false)
    {
        if (AttackSpawn != null && target != null && lastShot <= 0)
        {
            StopAllCoroutines();
            Idle();

            transform.LookAt(target.transform);

            AttackSpawn.Void((int)(GetAttack() * (upgraded ? VoidUpgradedPower : VoidPower)), target.transform, upgraded);

            lastShot = VoidFireRate;
            lastHit = 0;
        }
    }

    private void Shield(bool upgraded = false)
    {
        Audio.PlayOneShot(ShieldSound);
        CurrentArmor = Mathf.Clamp(CurrentArmor + MaxHP * (upgraded ? ShieldUpgradedPotency : ShieldPotency), 0, MaxHP);
        UpdateArmorBar(true);
    }

    public override void Die()
    {
        FindObjectOfType<Game>().PlayerDeath();
    }
}
