﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour
{
    [SerializeField] private List<EnvironmentTile> Walls;
    [SerializeField] private List<EnvironmentTile> Floors;
    [SerializeField] private List<EnvironmentTile> Lakes;

    [SerializeField] private GameObject Door;

    private List<List<GameObject>> Decor = new List<List<GameObject>>();
    [SerializeField] private List<GameObject> LakeDecor;

    [SerializeField] private List<GameObject> WallDecor;

    [SerializeField] private List<GameObject> DecorStone;
    [SerializeField] private List<GameObject> DecorNature;
    [SerializeField] private List<GameObject> DecorEarth;
    [SerializeField] private List<GameObject> DecorCave;
    [SerializeField] private List<GameObject> DecorWood;

    private List<List<GameObject>> Objects = new List<List<GameObject>>();

    [SerializeField] private List<GameObject> ObjectStone;
    [SerializeField] private List<GameObject> ObjectNature;
    [SerializeField] private List<GameObject> ObjectEarth;
    [SerializeField] private List<GameObject> ObjectCave;
    [SerializeField] private List<GameObject> ObjectWood;

    [SerializeField] private GameObject Soul;
    [SerializeField] private List<GameObject> Monsters;
    [SerializeField] private List<GameObject> LakeMonsters;
    [SerializeField] private List<GameObject> RareMonsters;
    [SerializeField] private List<GameObject> RareLakeMonsters;
    [SerializeField] private float DecorRatio;
    [SerializeField] private float LakeDecorRatio;
    [SerializeField] private float WallDecorRatio;
    [SerializeField] private float ObjectRatio;

    [SerializeField] private Portal Portal;
    [SerializeField] private GameObject SpawnEffect;

    [SerializeField] public TaskController Task;

    [SerializeField] private Vector2Int Size;
    [SerializeField] private Vector2Int Rooms;
    [SerializeField] private Vector2Int RoomSize;
    [SerializeField] private Vector2Int CorridorLength;
    
    [SerializeField] private Vector2 LakeRatio;
    [SerializeField] private Vector2 LakeSpread;
    [SerializeField] private Vector2 NearLakeSpread;
    [SerializeField] private Vector2 NearLakeSpread2;

    [SerializeField] private Vector2 CaveRatio;
    [SerializeField] private Vector2 CaveSpread;

    [SerializeField] private Vector2 WoodRatio;

    [SerializeField] private Vector2 MonsterRatio;
    [SerializeField] private Vector2 RareMonsterRatio;

    public EnvironmentTile[][] mMap;
    private List<EnvironmentTile> mAll;
    private List<EnvironmentTile> mToBeTested;
    private List<EnvironmentTile> mLastSolution;
    private List<GameObject> mObjects = new List<GameObject>();
    private List<GameObject> mMonsters = new List<GameObject>();

    public STile[,] tileMap;
    public List<SRoom> roomsCreated;
    private float rareMonsterChance;
    private int startRoom;

    private readonly Vector3 NodeSize = Vector3.one * 9.0f; 
    private const float TileSize = 10.0f;
    private const float TileHeight = 2.5f;

    public EnvironmentTile StartTile { get; private set; }
    public EnvironmentTile GoalTile { get; private set; }

    public int CurrentFloor = 0;

    private Vector2Int xRange;
    private Vector2Int yRange;
    public EnvironmentTile MidTile;
    [SerializeField] private Camera MapCamera;

    private void Start()
    {
        Decor.Add(DecorStone);
        Decor.Add(DecorNature);
        Decor.Add(DecorEarth);
        Decor.Add(DecorCave);
        Decor.Add(DecorWood);

        Objects.Add(ObjectStone);
        Objects.Add(ObjectNature);
        Objects.Add(ObjectEarth);
        Objects.Add(ObjectCave);
        Objects.Add(ObjectWood);
    }

    private void Awake()
    {
        mAll = new List<EnvironmentTile>();
        mToBeTested = new List<EnvironmentTile>();
    }

    private void OnDrawGizmos()
    {
        // Draw the environment nodes and connections if we have them
        if (mMap != null)
        {
            for (int x = 0; x < Size.x; ++x)
            {
                for (int y = 0; y < Size.y; ++y)
                {
                    if (mMap[x][y].Connections != null)
                    {
                        for (int n = 0; n < mMap[x][y].Connections.Count; ++n)
                        {
                            Gizmos.color = Color.blue;
                            Gizmos.DrawLine(mMap[x][y].Position, mMap[x][y].Connections[n].Position);
                        }
                    }

                    // Use different colours to represent the state of the nodes
                    Color c = Color.white;
                    if ( !mMap[x][y].IsAccessible )
                    {
                        c = Color.red;
                    }
                    else
                    {
                        if(mLastSolution != null && mLastSolution.Contains( mMap[x][y] ))
                        {
                            c = Color.green;
                        }
                        else if (mMap[x][y].Visited)
                        {
                            c = Color.yellow;
                        }
                    }

                    Gizmos.color = c;
                    Gizmos.DrawWireCube(mMap[x][y].Position, NodeSize);
                }
            }
        }
    }

    // Enum to hold the type of tiles that make up the map
    [System.Serializable]
    public enum ETileType
    {
        Wall,
        Floor,
        Corridor,
        Start,
        Goal,
        Lake,
        Cave,
        Wood,
        Soul,
        Monster,
        Doorway
    }

    public enum ETerrainType
    {
        Stone = 0,
        Nature = 1,
        Earth = 2,
        Cave = 3,
        Wood = 4
    }

    [System.Serializable]
    public enum EMonsterType
    {
        None,
        Normal,
        Lake,
        RareNormal,
        RareLake
    }

    // Enum to hold possible directions
    [System.Serializable]
    public enum EDirection
    {
        North = 0,
        East = 1,
        South = 2, 
        West = 3,
        NoDirection = 4
    }

    // Struct representing a singular tile
    [System.Serializable]
    public struct STile
    {
        public ETileType type { get; set; }
        public ETerrainType terrainType { get; set; }
        public bool keepClear { get; set; }
        public bool hasObject { get; set; }
        public bool hasDecor { get; set; }
        public bool hasWallDecor { get; set; }
        public int corridorDirX { get; set; }
        public int corridorDirY { get; set; }
        public EMonsterType monsterType { get; set; }
        public int roomIndex { get; set; }
        public STile(ETileType tileType)
        {
            type = tileType;
            terrainType = ETerrainType.Stone;
            keepClear = false;
            hasObject = false;
            hasDecor = false;
            hasWallDecor = false;
            corridorDirX = 0;
            corridorDirY = 0;
            monsterType = EMonsterType.None;
            roomIndex = 0;
        }
    }

    public class PathTile
    {
        public int x { get; set; }
        public int y { get; set; }
        public bool clear { get; set; }
        public int distance { get; set; }
        public PathTile parent { get; set; }

        public PathTile(int xPos, int yPos, bool isClear)
        {
            x = xPos;
            y = yPos;
            clear = isClear;
        }

        private int GetDistance(PathTile goal)
        {
            return Mathf.Abs(goal.x - x) + Mathf.Abs(goal.y - y);
        }

        public bool FindPath(List<List<PathTile>> room, PathTile goal, ref List<PathTile> path, bool ignoreAccessibility = false)
        {
            path.Clear();

            PathTile cn;
            List<PathTile> ol = new List<PathTile>();
            List<PathTile> cl = new List<PathTile>();

            distance = GetDistance(goal);
            parent = null;
            ol.Add(this);

            do
            {
                int index = Random.Range(0, ol.Count);
                cn = ol[index];
                ol.RemoveAt(index);

                if (cn.distance == 0)
                {
                    while (cn.parent != null)
                    {
                        path.Add(cn);
                        cn = cn.parent;
                    }
                    path.Add(cn);
                    return true;
                }

                for (int i = -1; i < 2; i++) for (int j = -1; j < 2; j++)
                {
                    if ((i == 0 || j == 0) && i != j && 
                        cn.x + i < room.Count && cn.x + i >= 0 && cn.y + j < room[0].Count && cn.y + j >= 0 &&
                        (ignoreAccessibility || room[cn.x + i][cn.y + j].clear))
                    {
                        PathTile newTile = room[cn.x + i][cn.y + j];

                        bool isNew = true;
                        for (int k = 0; k < ol.Count && isNew; k++) if (ol[k].x == newTile.x && ol[k].y == newTile.y) isNew = false;
                        for (int k = 0; k < cl.Count && isNew; k++) if (cl[k].x == newTile.x && cl[k].y == newTile.y) isNew = false;

                        if (isNew)
                        {
                            newTile.distance = newTile.GetDistance(goal);
                            newTile.parent = cn;
                            ol.Add(newTile);
                        }
                    }
                }

                cl.Add(cn);
            } 
            while (ol.Count > 0);

            return false;
        }

        public void MakePath(List<List<PathTile>> room, PathTile target)
        {
            List<PathTile> path = new List<PathTile>();
            if (!FindPath(room, target, ref path))
            {
                FindPath(room, target, ref path, true);
                for (int i = 0; i < path.Count; i++) path[i].clear = true;
            }
        }
    }

    // Struct representing a single room on the map
    [System.Serializable]
    public struct SRoom
    {
        public int sizeX { get; set; } // Size of the room
        public int sizeY { get; set; } 
        public int posX { get; set; } // Position of the room
        public int posY { get; set; }
        public List<EDirection> freeSides { get; set; } // Directions at which corridors can be linked
        public bool hasLake { get; set; }
        
        public SRoom(Vector2Int roomSize, Vector2Int roomPosition, EDirection corridorDirection)
        {
            sizeX = roomSize.x;
            sizeY = roomSize.y;
            posX = roomPosition.x;
            posY = roomPosition.y;
            freeSides = new List<EDirection>{ EDirection.North, EDirection.East, EDirection.South, EDirection.West };
            hasLake = false;

            if (corridorDirection != EDirection.NoDirection) RemoveSide(corridorDirection);
        }

        public void RemoveSide(EDirection side)
        {
            freeSides.Remove(side);
        }
    }

    private void ClearPaths()
    {
        // Mark all tiles that must be accessible
        for (int x = 1; x < Size.x - 1; x++)
        {
            for (int y = 1; y < Size.y - 1; y++)
            {
                // If there's something special on the tile it has to be kept free
                if (tileMap[x, y].type != ETileType.Floor) tileMap[x, y].keepClear = true;

                // Same if there is a corridor nearby
                for (int i = -1; i < 2 && !tileMap[x, y].keepClear; i++) for (int j = -1; j < 2; j++)
                {
                    if ((i == 0 || j == 0) && (tileMap[x + i, y + j].type == ETileType.Corridor || tileMap[x + i, y + j].type == ETileType.Doorway))
                    {
                        tileMap[x, y].keepClear = true;
                    }
                }
            }
        }

        //Go through rooms and ensure all marked tiles have a clear path between them
        for (int i = 0; i < roomsCreated.Count; i++)
        {
            List<List<PathTile>> room = new List<List<PathTile>>(); // All tiles in this room
            List<PathTile> pathPoints = new List<PathTile>(); // Tiles that must be accessible

            // Fill the above lists
            for (int x = roomsCreated[i].posX; x < roomsCreated[i].posX + roomsCreated[i].sizeX; x++)
            {
                List<PathTile> currentColumn = new List<PathTile>();
                for (int y = roomsCreated[i].posY; y < roomsCreated[i].posY + roomsCreated[i].sizeY; y++)
                {
                    bool free = tileMap[x, y].keepClear;
                    PathTile thisTile = new PathTile(x - roomsCreated[i].posX, y - roomsCreated[i].posY, free);

                    currentColumn.Add(thisTile);
                    if (free) pathPoints.Add(thisTile);
                }
                room.Add(currentColumn);
            }

            if (pathPoints.Count < 2)
            {
                // If the room only has one clear tile, add one more to make sure the room is not blocked right at the entrance
                pathPoints.Add(room[Random.Range(1, room.Count - 2)][Random.Range(1, room[0].Count - 2)]);
            }

            // Check if all pathPoints can access another and if not create paths between them
            for (int j = 0; j < pathPoints.Count; j++)
            {
                for (int k = j; k < pathPoints.Count; k++)
                {
                    if (j != k)
                    {
                        pathPoints[j].MakePath(room, pathPoints[k]);
                    }
                }
            }

            // Reflect that on the actual tiles
            for (int x = roomsCreated[i].posX; x < roomsCreated[i].posX + roomsCreated[i].sizeX; x++)
            {
                for (int y = roomsCreated[i].posY; y < roomsCreated[i].posY + roomsCreated[i].sizeY; y++)
                {
                    tileMap[x, y].keepClear = room[x - roomsCreated[i].posX][y - roomsCreated[i].posY].clear;
                }
            }
        }
    }

    private float nearSpread = 0;
    private float nearSpread2 = 0;
    private void Spread(int startX, int startY, float maxSpread, bool lake = true, ETerrainType terrain = ETerrainType.Stone, int it = 0)
    {
        if (startX > 0 && startX < Size.x - 1 && startY > 0 && startY < Size.y - 1)
        {
            if ((((lake && !tileMap[startX, startY].keepClear) || !lake) && // When spreading lake make sure it doesn't obstruct any important paths
                tileMap[startX, startY].type != ETileType.Wall) || // Lake and near lake tiles shouldn't "flood" outside the walls
                (!lake && tileMap[startX, startY].terrainType == ETerrainType.Cave)) // Cave terrain can affect walls
            {
                if (lake) tileMap[startX, startY].type = ETileType.Lake;
                if (!(terrain == ETerrainType.Earth && tileMap[startX, startY].terrainType == ETerrainType.Nature)) 
                    tileMap[startX, startY].terrainType = terrain;

                for (int i = -1; i < 2; i++) for (int j = -1; j < 2; j++)
                        if ((i == 0 || j == 0) && i != j)
                        {
                            if (tileMap[startX + i, startY + j].type == ETileType.Wall && 
                                (!(terrain == ETerrainType.Earth && tileMap[startX + i, startY + j].terrainType == ETerrainType.Nature)
                                && tileMap[startX + i, startY + j].terrainType != ETerrainType.Cave))
                            {
                                // Spread affects the nearby walls
                                tileMap[startX + i, startY + j].terrainType = terrain;
                            }

                            if (Random.value <= 1 - (it * (1.0f / (float)maxSpread)))
                            {
                                // Spread the lake or terrain
                                Spread(startX + i, startY + j, maxSpread, lake, terrain, it + 1);
                            }
                            else if (lake)
                            {
                                // Spread layer of terrain around the lake
                                if (nearSpread == 0) nearSpread = Random.Range(NearLakeSpread.x, NearLakeSpread.y);
                                Spread(startX + i, startY + j, nearSpread, false, terrain, 0);
                            }
                            else if (terrain == ETerrainType.Nature)
                            {
                                // Another layer of terrain
                                if (nearSpread2 == 0) nearSpread2 = Random.Range(NearLakeSpread2.x, NearLakeSpread2.y);
                                Spread(startX + i, startY + j, nearSpread2, false, ETerrainType.Earth, 0);
                            }
                        }
            }
        }
    }

    private void PlaceInRooms(ETileType type, int number, bool onePerRoom, int roomToExclude = -1)
    {
        List<int> roomsUsed = new List<int>();
        SRoom targetRoom;


        for (int i = 0; i < number; i++)
        {
            int roomIndex = Random.Range(0, roomsCreated.Count);

            int it = 0;
            while ((onePerRoom && roomsUsed.Contains(roomIndex)) || roomIndex == roomToExclude && it < 20)
            {
                roomIndex++;
                if (roomIndex == roomsCreated.Count) roomIndex = 0;
                it++;
            }

            targetRoom = roomsCreated[roomIndex];

            Vector2Int tilePos;
            it = 0;

            switch (type)
            {
                case ETileType.Start:
                {
                    bool good;
                    do
                    {
                        good = true;
                        tilePos = new Vector2Int(Random.Range(targetRoom.posX, targetRoom.posX + targetRoom.sizeX), Random.Range(targetRoom.posY, targetRoom.posY + targetRoom.sizeY));
                        if (tileMap[tilePos.x, tilePos.y].type != ETileType.Floor) good = false;
                        else for (int x = -1; x < 2; x++) for (int y = -1; y < 2; y++)
                        {
                            if (tileMap[tilePos.x + x, tilePos.y + y].type == ETileType.Wall) good = false;
                        }
                        it++;
                    }
                    while (!good && it < 20);

                    if (good)
                    {
                        tileMap[tilePos.x, tilePos.y].type = ETileType.Start;
                        startRoom = roomIndex;
                        Debug.Log("Start position assigned to " + tilePos.x + ", " + tilePos.y);
                    }
                    else i--;
                    break;
                }
                case ETileType.Goal:
                {
                    bool good;
                    do
                    {
                        good = true;
                        tilePos = new Vector2Int(Random.Range(targetRoom.posX, targetRoom.posX + targetRoom.sizeX), Random.Range(targetRoom.posY, targetRoom.posY + targetRoom.sizeY));
                        if (tileMap[tilePos.x, tilePos.y].type != ETileType.Floor) good = false;
                        else for (int x = -1; x < 2; x++) for (int y = -1; y < 2; y++)
                        {
                            if (tileMap[tilePos.x + x, tilePos.y + y].type == ETileType.Wall) good = false;
                        }
                        it++;
                    }
                    while (!good && it < 20);

                    if (good)
                    {
                        tileMap[tilePos.x, tilePos.y].type = ETileType.Goal;
                        Debug.Log("End position assigned to " + tilePos.x + ", " + tilePos.y);
                    }
                    else i--;
                    break;
                }
                case ETileType.Soul:
                {
                    bool good = true;
                    do
                    {
                        tilePos = new Vector2Int(Random.Range(targetRoom.posX + 1, targetRoom.posX + targetRoom.sizeX - 1), Random.Range(targetRoom.posY + 1, targetRoom.posY + targetRoom.sizeY - 1));

                        // Don't spawn souls too close to one another
                        for (int x = -1; x < 2; x++) for (int y = -1; y < 2; y++)
                        {
                            if (tileMap[tilePos.x + x, tilePos.y + y].type == ETileType.Soul) good = false;
                        }
                        
                        it++;
                    }
                    while (!good && tileMap[tilePos.x, tilePos.y].type != ETileType.Floor && it < 20);

                    if (good && tileMap[tilePos.x, tilePos.y].type == ETileType.Floor) tileMap[tilePos.x, tilePos.y].type = ETileType.Soul;
                    else i--;
                    break;
                }

                case ETileType.Lake:
                {
                    do
                    {
                        it++;
                        tilePos = new Vector2Int(Random.Range(targetRoom.posX, targetRoom.posX + targetRoom.sizeX), Random.Range(targetRoom.posY, targetRoom.posY + targetRoom.sizeY));
                    }
                    while (tileMap[tilePos.x, tilePos.y].keepClear && it < 20); // Limiter to avoid infinite loops if no tile is free

                    if (!tileMap[tilePos.x, tilePos.y].keepClear)
                    {
                        ETerrainType terrain = Random.value < 0.5f ? ETerrainType.Nature : ETerrainType.Earth;
                        Spread(tilePos.x, tilePos.y, Random.Range(LakeSpread.x, LakeSpread.y), true, terrain);
                        nearSpread = 0;
                        nearSpread2 = 0;
                        targetRoom.hasLake = true;
                        roomsCreated[roomIndex] = targetRoom;
                    }
                    else i--;

                    break;
                }

                case ETileType.Cave:
                {
                    tilePos = new Vector2Int(Random.Range(targetRoom.posX, targetRoom.posX + targetRoom.sizeX), Random.Range(targetRoom.posY, targetRoom.posY + targetRoom.sizeY));
                    
                    Spread(tilePos.x, tilePos.y, Random.Range(CaveSpread.x, CaveSpread.y), false, ETerrainType.Cave);
                    nearSpread = 0;
                    nearSpread2 = 0;

                    break;
                }

                case ETileType.Wood:
                {
                    if (targetRoom.hasLake)
                    {
                        i--;
                        break;
                    }
                    else
                    {
                        // Turn all the room tiles and surrounding walls and corridors into wood
                        for (int x = targetRoom.posX - 1; x <= targetRoom.posX + targetRoom.sizeX; x++) for (int y = targetRoom.posY - 1; y <= targetRoom.posY + targetRoom.sizeY; y++)
                        {
                            if (x == targetRoom.posX - 1 || x == targetRoom.posX + targetRoom.sizeX || y == targetRoom.posY - 1 || y == targetRoom.posY + targetRoom.sizeY)
                            {
                                if (tileMap[x, y].type != ETileType.Floor)
                                {
                                    tileMap[x, y].terrainType = ETerrainType.Wood;
                                        if (tileMap[x, y].type == ETileType.Corridor)
                                        {
                                            int walls = 0;
                                            for (int j = -1; j < 2; j++) for (int k = -1; k < 2; k++) if (j != k && (j == 0 || k == 0) && tileMap[x + j, y + k].type == ETileType.Wall) walls++;
                                            if (walls == 2) tileMap[x, y].type = ETileType.Doorway;
                                        }
                                }
                            }
                            else tileMap[x, y].terrainType = ETerrainType.Wood;
                        }
                    }

                    break;
                }

                case ETileType.Monster:
                {
                    bool good = false;
                    do
                    {
                        it++;
                        tilePos = new Vector2Int(Random.Range(targetRoom.posX, targetRoom.posX + targetRoom.sizeX), Random.Range(targetRoom.posY, targetRoom.posY + targetRoom.sizeY));
                        if (tileMap[tilePos.x, tilePos.y].type == ETileType.Floor)
                        {
                            if (tileMap[tilePos.x, tilePos.y].keepClear) good = true;
                            else for (int x = -1; x < 2; x++) for (int y = -1; y < 2; y++)
                            {
                                if ((x == 0 || y == 0) && tileMap[tilePos.x + x, tilePos.y + y].keepClear && tileMap[tilePos.x + x, tilePos.y + y].type != ETileType.Wall && tileMap[tilePos.x + x, tilePos.y + y].type != ETileType.Lake) 
                                    good = true;
                            }
                        }
                    }
                    while (!good && it < 20);

                    if (good)
                    {
                        tileMap[tilePos.x, tilePos.y].type = ETileType.Monster;
                        
                        if ((Random.Range(0.0f, 1.0f) < rareMonsterChance)) tileMap[tilePos.x, tilePos.y].monsterType = targetRoom.hasLake ? EMonsterType.RareLake : EMonsterType.RareNormal;
                        else tileMap[tilePos.x, tilePos.y].monsterType = targetRoom.hasLake ? EMonsterType.Lake : EMonsterType.Normal;
                    }
                    else i--;

                    break;
                }
            }
            
            if (onePerRoom)
            {
                roomsUsed.Add(roomIndex);
                if (roomsUsed.Count == (roomToExclude >= 0 ? roomsCreated.Count - 1 : roomsCreated.Count)) roomsUsed.Clear();
            }
        }
    }

    private void GenerateObjects()
    {
        for (int i = 0; i < roomsCreated.Count; i++)
        {
            for (int x = roomsCreated[i].posX; x < roomsCreated[i].posX + roomsCreated[i].sizeX; x++)
            {
                for (int y = roomsCreated[i].posY; y < roomsCreated[i].posY + roomsCreated[i].sizeY; y++)
                {
                    if (tileMap[x, y].type == ETileType.Floor || tileMap[x, y].type == ETileType.Lake && (x > 0 && x < Size.x - 1 && y > 0 && y < Size.y - 1))
                    {
                        float byWall = 2.5f; // If the tile isn't next to a wall, decrease odds of spawning object or decor
                        bool corridor = false; // Guarantees wall decors next to corridor entrances

                        for (int j = -1; j < 2; j++) for (int k = -1; k < 2; k++)
                        {
                                if ((j == 0 || k == 0) && tileMap[x + j, y + k].type == ETileType.Wall)
                                    byWall = 0.8f;
                                else if ((j != 0 && k != 0) && (tileMap[x + j, y + k].type == ETileType.Corridor || tileMap[x + j, y + k].type == ETileType.Doorway))
                                {
                                    corridor = true;
                                    tileMap[x, y].corridorDirX = j;
                                    tileMap[x, y].corridorDirY = k;
                                }
                        }

                        if (!tileMap[x, y].keepClear && tileMap[x, y].type == ETileType.Floor)
                        {
                            tileMap[x, y].hasObject = Random.value * byWall < ObjectRatio;

                            // Avoid spawning objects next to each other if not by wall
                            for (int j = -1; j < 2 && tileMap[x, y].hasObject; j++) for (int k = -1; k < 2; k++)
                            {
                                if (!(j == 0 && k == 0) && (byWall > 1.0f && tileMap[x + j, y + k].hasObject))
                                    tileMap[x, y].hasObject = false;
                            }
                        }

                        // Decor
                        if (!tileMap[x, y].hasObject && byWall < 1.0f && corridor) tileMap[x, y].hasWallDecor = true;
                        else if (!tileMap[x, y].hasObject && Random.value * byWall < DecorRatio)
                        {
                            tileMap[x, y].hasDecor = true;
                            if (byWall < 1.0f && Random.value * byWall < WallDecorRatio) tileMap[x, y].hasWallDecor = true;
                        }

                        // Prevent consecutive wall decors
                        if (tileMap[x, y].hasWallDecor)
                        {
                            for (int j = -1; j < 2; j++) for (int k = -1; k < 2; k++)
                            {
                                if ((j == 0 || k == 0) && j != k && tileMap[x+j, y+k].hasWallDecor)
                                {
                                    if (!corridor) tileMap[x, y].hasWallDecor = false;
                                    else if (tileMap[x + j, y + k].corridorDirX == 0) tileMap[x + j, y + k].hasWallDecor = false;
                                }
                            }
                        }
                    }
                    else if (tileMap[x, y].type == ETileType.Lake)
                    {
                        float red = 0.5f; // If the tile has water around it, increase chances of spawning lake decor

                        for (int j = -1; j < 2; j++) for (int k = -1; k < 2; k++)
                            {
                                if (tileMap[x + j, y + k].type == ETileType.Lake)
                                    red += 0.3f;
                            }

                        if (Random.value / red < LakeDecorRatio) tileMap[x, y].hasDecor = true;
                    }
                }
            }
        }
    }

    private void GenerateBase()
    {
        // Create an array to hold the map tile information and fill it with walls for now
        tileMap = new STile[Size.x, Size.y];
        for (int i = 0; i < Size.x; i++)
        {
            for (int j = 0; j < Size.y; j++)
            {
                tileMap[i, j] = new STile(ETileType.Wall);
            }
        }

        // Determine the number of rooms to generate and make an array to contain generated rooms
        int totalRooms = Random.Range(Rooms.x, Rooms.y + 1);
        roomsCreated = new List<SRoom>();

        // Values for the first room
        Vector2Int roomPos = new Vector2Int(Random.Range(1, Size.x - 1), Random.Range(1, Size.y - 1));

        EDirection corridorDirection = EDirection.NoDirection;
        Vector2Int roomSize = new Vector2Int(Random.Range(RoomSize.x, RoomSize.y + 1), Random.Range(RoomSize.x, RoomSize.y + 1));

        if (roomPos.x + roomSize.x >= Size.x - 1) roomPos.x = roomPos.x - roomSize.x;
        if (roomPos.y + roomSize.y >= Size.y - 1) roomPos.y = roomPos.y - roomSize.y;

        int roomsToCheck = 0;

        // Generate rooms and the corridors between them
        do
        {
            // Add a room to the list
            roomsCreated.Add(new SRoom(roomSize, roomPos, corridorDirection));
            roomsToCheck++;

            Debug.Log("Creating room number " + (roomsCreated.Count - 1).ToString());
            for (int i = roomPos.x; i < roomPos.x + roomSize.x; i++)
            {
                for (int j = roomPos.y; j < roomPos.y + roomSize.y; j++)
                {
                    tileMap[i, j].type = ETileType.Floor;
                    tileMap[i, j].roomIndex = roomsCreated.Count - 1;
                }
            }

            // Determine the size of the next room and length of the corridor leading to it
            roomSize = new Vector2Int(Random.Range(RoomSize.x, RoomSize.y + 1), Random.Range(RoomSize.x, RoomSize.y + 1));
            int corridorLength = Random.Range(CorridorLength.x, CorridorLength.y + 1);

            // Create a corridor branching from one of the rooms, making sure it ends in an unocuppied spot
            if (roomsCreated.Count < totalRooms)
            {
                while (roomsToCheck > 0)
                {
                    // Select a random room to create a corridor from
                    int originIndex = (roomsToCheck > 1) ? Random.Range(0, roomsToCheck) : 0;

                    int it = 0;
                    while (roomsCreated[originIndex].freeSides.Count == 0 && it < roomsCreated.Count)
                    {
                        originIndex++;
                        if (originIndex >= roomsCreated.Count) originIndex = 0;
                        it++;
                    }
                    if (roomsCreated[originIndex].freeSides.Count > 0)
                    {
                        SRoom originRoom = roomsCreated[originIndex];

                        bool free = false;

                        // See if the corridor and the room can be inserted in any of the free directions
                        while (!free && originRoom.freeSides.Count > 0)
                        {
                            EDirection currentDirection = originRoom.freeSides[Random.Range(0, originRoom.freeSides.Count)];

                            free = true;

                            int exit = -1;
                            int yPos = -1;
                            int xPos = -1;

                            // Get a position of the new room
                            switch (currentDirection)
                            {
                                case EDirection.North:
                                {
                                    exit = Random.Range(originRoom.posX, originRoom.posX + originRoom.sizeX);
                                    xPos = Random.Range(exit - roomSize.x, exit) + 1;

                                    yPos = originRoom.posY + originRoom.sizeY + corridorLength;

                                    break;
                                }
                                case EDirection.East:
                                {
                                    exit = Random.Range(originRoom.posY, originRoom.posY + originRoom.sizeY);
                                    yPos = Random.Range(exit - roomSize.y, exit) + 1;

                                    xPos = originRoom.posX + originRoom.sizeX + corridorLength;

                                    break;
                                }
                                case EDirection.South:
                                {
                                    exit = Random.Range(originRoom.posX, originRoom.posX + originRoom.sizeX - 1);
                                    xPos = Random.Range(exit - roomSize.x, exit) + 1;

                                    yPos = originRoom.posY - roomSize.y - corridorLength;

                                    break;
                                }
                                case EDirection.West:
                                {
                                    exit = Random.Range(originRoom.posY, originRoom.posY + originRoom.sizeY - 1);
                                    yPos = Random.Range(exit - roomSize.y, exit) + 1;

                                    xPos = originRoom.posX - roomSize.x - corridorLength;

                                    break;
                                }
                            }

                            // Check if the room can be created there
                            roomPos = new Vector2Int(xPos, yPos);

                            for (int i = roomPos.x; i < roomPos.x + roomSize.x && free; i++)
                            {
                                for (int j = roomPos.y; j < roomPos.y + roomSize.y; j++)
                                {
                                    if (i <= 1 || i >= Size.x - 2 || j <= 1 || j >= Size.y - 2 || tileMap[i, j].type != ETileType.Wall)
                                    {
                                        free = false;
                                        break;
                                    }
                                }
                            }

                            // If all checks succeed create a corridor in the given direction
                            if (free)
                            {
                                originRoom.RemoveSide(currentDirection);
                                corridorDirection = (EDirection)(((int)currentDirection + 2) % 4);

                                Debug.Log("Creating corridor");

                                switch (currentDirection)
                                {
                                    case EDirection.North:
                                    {
                                        for (int i = 0; i < corridorLength; i++)
                                        {
                                            tileMap[exit, originRoom.posY + originRoom.sizeY + i].type = ETileType.Corridor;
                                        }
                                        break;
                                    }
                                    case EDirection.East:
                                    {
                                        for (int i = 0; i < corridorLength; i++)
                                        {
                                            tileMap[originRoom.posX + originRoom.sizeX + i, exit].type = ETileType.Corridor;
                                        }
                                        break;
                                    }
                                    case EDirection.South:
                                    {
                                        for (int i = 0; i < corridorLength; i++)
                                        {
                                            tileMap[exit, originRoom.posY - i - 1].type = ETileType.Corridor;
                                        }
                                        break;
                                    }
                                    case EDirection.West:
                                    {
                                        for (int i = 0; i < corridorLength; i++)
                                        {
                                            tileMap[originRoom.posX - i - 1, exit].type = ETileType.Corridor;
                                        }
                                        break;
                                    }
                                }
                            }

                            // Otherwise remove this direction from the origin room's free sides
                            else
                            {
                                originRoom.RemoveSide(currentDirection);
                            }
                        }

                        // End loop if a free space was found
                        if (free) break;

                        // Otherwise decrease the number of rooms available
                        roomsToCheck--;
                    }
                    else
                    {
                        Debug.Log("Error, starting over...");
                        GenerateBase(); // Try again if there isn't enough space to make enough rooms
                        roomsToCheck = 0;
                        break;
                    }
                }
            }
        }
        while (roomsCreated.Count < totalRooms && roomsToCheck > 0);

        Debug.Log("Base dungeon generated");
    }

    private void Generate()
    {
        // Start by determining the task that needs to be completed to proceed, as this might affect the later steps
        int requirement = 0;
        Task.NewTask(ref requirement);

        // Generate rooms and corridors
        GenerateBase();

        // Determine start and end positions
        PlaceInRooms(ETileType.Start, 1, false);
        PlaceInRooms(ETileType.Goal, 1, false, startRoom);

        // Place souls if task requires them
        if (Task.type == TaskController.ETaskType.SoulCollect)
        {
            PlaceInRooms(ETileType.Soul, requirement, true);
            Debug.Log(requirement + " soul positions marked");
        }

        // Mark the tiles that can't have anything placed on them
        ClearPaths();
        Debug.Log("Paths cleared");

        // Turn some of the terrain into caves 
        float caveRatio = Random.Range(CaveRatio.x, CaveRatio.y);
        int caveCount = (int)(roomsCreated.Count * caveRatio);

        PlaceInRooms(ETileType.Cave, caveCount, false);

        // Create lakes
        float lakeRatio = Random.Range(LakeRatio.x, LakeRatio.y);
        int lakeCount = (int)(roomsCreated.Count * lakeRatio);

        PlaceInRooms(ETileType.Lake, lakeCount, false);
        Debug.Log("Lake positions marked");

        // Make the walls and floors in some rooms wooden
        float woodRatio = Random.Range(WoodRatio.x, WoodRatio.y);
        int woodCount = (int)(roomsCreated.Count * woodRatio);

        PlaceInRooms(ETileType.Wood, woodCount, false);

        // Spawn monsters
        float monsterRatio = Random.Range(MonsterRatio.x, MonsterRatio.y);
        int monsterCount = (int)(roomsCreated.Count * monsterRatio);
        if (Task.type == TaskController.ETaskType.MonsterKill && monsterCount < requirement) monsterCount = requirement;
        rareMonsterChance = Random.Range(RareMonsterRatio.x, RareMonsterRatio.y);

        PlaceInRooms(ETileType.Monster, monsterCount, true, startRoom);

        Debug.Log(monsterCount + " monster positions marked with a " + rareMonsterChance + " chance of rare monsters");

        // Determine where to put objects
        GenerateObjects();
        Debug.Log("Object positions marked.");

        Debug.Log("Dungeon generation complete.");
    }

    public void BuildWorld()
    {
        CleanUpWorld();

        Debug.Log("Building world...");

        mMap = new EnvironmentTile[Size.x][];
        bool start = false;
        bool goal = false;

        int halfWidth = Size.x / 2;
        int halfHeight = Size.y / 2;
        Vector3 position = new Vector3(-(halfWidth * TileSize), 0.0f, -(halfHeight * TileSize));

        xRange = new Vector2Int(halfWidth, halfWidth);
        yRange = new Vector2Int(halfHeight, halfHeight);

        float discoverableTiles = 0;

        for (int x = 0; x < Size.x; ++x)
        {
            mMap[x] = new EnvironmentTile[Size.y];
            for (int y = 0; y < Size.y; ++y)
            {
                // Count non-wall tiles and keep track of the furthest ones
                if (tileMap[x, y].type != ETileType.Wall)
                {
                    discoverableTiles++;

                    if (x < xRange.x) xRange.x = x;
                    else if (x > xRange.y) xRange.y = x;

                    if (y < yRange.x) yRange.x = y;
                    else if (y > yRange.y) yRange.y = y;
                }

                bool isAccessible = (tileMap[x, y].type != ETileType.Wall);
                bool reveal = (tileMap[x, y].type == ETileType.Wall);

                EnvironmentTile prefab = Floors[(int)tileMap[x, y].terrainType];

                GameObject newObject = null;

                switch (tileMap[x, y].type)
                {
                    case ETileType.Wall:
                    {
                        prefab = Walls[(int)tileMap[x, y].terrainType];
                        break;
                    }
                    case ETileType.Floor:
                    {
                        // Create objects and decor
                        if (tileMap[x, y].hasObject)
                        {
                            List<GameObject> objects = Objects[(int)tileMap[x, y].terrainType];
                            newObject = Instantiate(objects[Random.Range(0, objects.Count)], position, Quaternion.identity, transform);
                            newObject.gameObject.name = string.Format("Object({0},{1})", x, y);
                            mObjects.Add(newObject);
                            isAccessible = false;
                        }
                        else if (tileMap[x, y].hasWallDecor)
                        {
                            newObject = Instantiate(WallDecor[(int)tileMap[x, y].terrainType], position, Quaternion.identity, transform);
                            newObject.gameObject.name = string.Format("Wall Decor({0},{1})", x, y);
                            mObjects.Add(newObject);
                        }
                        else if (tileMap[x, y].hasDecor)
                        {
                            List<GameObject> decor = Decor[(int)tileMap[x, y].terrainType];
                            newObject = Instantiate(decor[Random.Range(0, decor.Count)], position, Quaternion.identity, transform);
                            newObject.gameObject.name = string.Format("Decor({0},{1})", x, y);
                            mObjects.Add(newObject);
                        }
                        break;
                    }
                    case ETileType.Corridor:
                    {
                        break;
                    }
                    case ETileType.Doorway:
                    {
                        newObject = Instantiate(Door, position, Quaternion.identity, transform);
                        newObject.gameObject.name = string.Format("Door({0},{1})", x, y);
                        mObjects.Add(newObject);
                        break;
                    }
                    case ETileType.Start:
                    {
                        start = true;
                        Instantiate(SpawnEffect, position, Quaternion.identity, transform);
                        break;
                    }
                    case ETileType.Goal:
                    {
                        goal = true;
                        Portal.transform.position = position;
                        Portal.Initialise();
                        break;
                    }
                    case ETileType.Lake:
                    {
                        prefab = Lakes[0];
                        isAccessible = false;

                        if (tileMap[x, y].hasWallDecor)
                        {
                            newObject = Instantiate(WallDecor[(int)tileMap[x, y].terrainType], position, Quaternion.identity, transform);
                            newObject.gameObject.name = string.Format("Wall Decor({0},{1})", x, y);
                            mObjects.Add(newObject);
                        }
                        else if (tileMap[x, y].hasDecor)
                        {
                            newObject = Instantiate(LakeDecor[Random.Range(0, LakeDecor.Count)], position, Quaternion.identity, transform);
                            newObject.gameObject.name = string.Format("Decor({0},{1})", x, y);
                            mObjects.Add(newObject);
                        }
                        break;
                    }
                    case ETileType.Soul:
                    {
                        newObject = Instantiate(Soul, position, Quaternion.identity, transform);
                        mObjects.Add(newObject);
                        newObject.gameObject.name = string.Format("Soul({0},{1})", x, y);
                        break;
                    }
                    case ETileType.Monster:
                    {
                        isAccessible = false;
                        break;
                    }
                    default:
                    {
                        prefab = Walls[0];
                        break;
                    }
                }

                EnvironmentTile tile = Instantiate(prefab, position, Quaternion.identity, transform);
                tile.Position = new Vector3(position.x + (TileSize / 2), TileHeight, position.z + (TileSize / 2));

                tile.HasObject = tileMap[x, y].hasObject;
                tile.IsAccessible = isAccessible;
                tile.MakeAccessible = !(tileMap[x, y].hasObject || tileMap[x, y].type == ETileType.Wall);
                tile.gameObject.name = string.Format("Tile({0},{1})", x, y);

                tile.Discovered = reveal;
                if (!reveal) tile.MapSetup();

                mMap[x][y] = tile;
                mAll.Add(tile);

                if (start)
                {
                    StartTile = tile;
                    start = false;
                    Debug.Log("Start position set.");
                }
                else if (goal)
                {
                    GoalTile = tile;
                    goal = false;
                    Portal.CurrentPosition = tile;
                    Debug.Log("Goal position set.");
                }
                else if (newObject != null)
                {
                    // Adjust position
                    newObject.transform.position = tile.Position;

                    if (tileMap[x, y].hasWallDecor || tileMap[x, y].hasObject)
                    {
                        // Rotate object depending on nearby walls
                        float rotation;

                        bool n = false;
                        bool e = false;
                        bool s = false;
                        bool w = false;

                        int walls = 0;

                        if (tileMap[x, y + 1].type == ETileType.Wall)
                        {
                            n = true;
                            walls++;
                        }
                        if (tileMap[x + 1, y].type == ETileType.Wall)
                        {
                            e = true;
                            walls++;
                        }
                        if (tileMap[x, y - 1].type == ETileType.Wall)
                        {
                            s = true;
                            walls++;
                        }
                        if (tileMap[x - 1, y].type == ETileType.Wall)
                        {
                            w = true;
                            walls++;
                        }

                        // If a wall decor is to be next to an entrance make sure it doesn't face against the wrong wall
                        if (tileMap[x, y].hasWallDecor && walls > 1 && tileMap[x, y].corridorDirX != 0)
                        {
                            if (n && tileMap[x, y].corridorDirY != 1) n = false;
                            if (s && tileMap[x, y].corridorDirY != -1) s = false;
                            if (e && tileMap[x, y].corridorDirX != 1) e = false;
                            if (w && tileMap[x, y].corridorDirX != -1) w = false;
                        }

                        if (!tileMap[x, y].hasWallDecor && n && e) rotation = 45;
                        else if (!tileMap[x, y].hasWallDecor && e && s) rotation = 135;
                        else if (!tileMap[x, y].hasWallDecor && s && w) rotation = 225;
                        else if (!tileMap[x, y].hasWallDecor && w && n) rotation = 315;
                        else if (n) rotation = 0;
                        else if (e) rotation = 90;
                        else if (s) rotation = 180;
                        else if (w) rotation = 270;
                        else rotation = Random.Range(0, 360);

                        newObject.transform.Rotate(new Vector3(0, rotation, 0));

                        if (tileMap[x, y].hasObject && walls > 1)
                        {
                            float offset = 0.7f;
                            newObject.transform.position += new Vector3((w ? offset : 0) + (e ? -offset : 0), 0, (n ? -offset : 0) + (s ? offset : 0));
                        }
                    }

                    else if (tileMap[x, y].type == ETileType.Doorway)
                    {
                        newObject.GetComponent<Door>().SetupDoor(tile);

                        // Rotate depending on nearby walls
                        float rotation = 0;

                        // True if the room is in that direction
                        bool n = false;
                        bool e = false;
                        bool s = false;
                        bool w = false;

                        if (tileMap[x, y + 1].type != ETileType.Wall && tileMap[x, y + 1].type != ETileType.Corridor)
                        {
                            n = true;
                        }
                        if (tileMap[x + 1, y].type != ETileType.Wall && tileMap[x + 1, y].type != ETileType.Corridor)
                        {
                            e = true;
                        }
                        if (tileMap[x, y - 1].type != ETileType.Wall && tileMap[x, y - 1].type != ETileType.Corridor)
                        {
                            s = true;
                        }
                        if (tileMap[x - 1, y].type != ETileType.Wall && tileMap[x - 1, y].type != ETileType.Corridor)
                        {
                            w = true;
                        }

                        if (s) rotation = 0;
                        else if (w) rotation = 90;
                        else if (n) rotation = 180;
                        else if (e) rotation = 270;

                        // Rotate by the given rotation
                        newObject.transform.Rotate(new Vector3(0, rotation, 0));
                    }
                }

                if (tileMap[x, y].type == ETileType.Monster)
                {
                    List<GameObject> monsters = Monsters;

                    switch (tileMap[x, y].monsterType)
                    {
                        case EMonsterType.Lake:
                        {
                            monsters = LakeMonsters;
                            break;
                        }
                        case EMonsterType.RareNormal:
                        {
                            monsters = RareMonsters;
                            break;
                        }
                        case EMonsterType.RareLake:
                        {
                            monsters = RareLakeMonsters;
                            break;
                        }
                    }

                    GameObject newMonster = Instantiate(monsters[Random.Range(0, monsters.Count)], position, Quaternion.identity, transform);

                    mMonsters.Add(newMonster);
                    newMonster.gameObject.name = string.Format("Monster({0},{1})", x, y);

                    newMonster.GetComponent<Monster>().MonsterSetup(CurrentFloor, tile, roomsCreated[tileMap[x, y].roomIndex]);
                }

                position.z += TileSize;
            }

            position.x += TileSize;
            position.z = -(halfHeight * TileSize);
        }

        EnvironmentTile MidTile = mMap[(xRange.x + xRange.y)/2][(yRange.x + yRange.y)/2];
        MapCamera.transform.position = new Vector3(MidTile.Position.x, 10, MidTile.Position.z);

        if (Task.type == TaskController.ETaskType.ChartMap)
        {
            Task.requirement = (int)(discoverableTiles * ((float)Task.requirement / 100.0f));
        }

        Debug.Log("Dungeon built. Seting up connections between the tiles...");
        SetupConnections();
        Debug.Log("Connections set up.");

        for (int i = 0; i < mMonsters.Count; i++)
            mMonsters[i].GetComponent<Monster>().StartPatrol();
        Debug.Log("Monsters started patrol.");
    }

    private void SetupConnections()
    {
        // Currently we are only setting up connections between adjacnt nodes
        for (int x = 0; x < Size.x; ++x)
        {
            for (int y = 0; y < Size.y; ++y)
            {
                EnvironmentTile tile = mMap[x][y];
                tile.Connections = new List<EnvironmentTile>();
                if (x > 0)
                {
                    tile.Connections.Add(mMap[x - 1][y]);
                }

                if (x < Size.x - 1)
                {
                    tile.Connections.Add(mMap[x + 1][y]);
                }

                if (y > 0)
                {
                    tile.Connections.Add(mMap[x][y - 1]);
                }

                if (y < Size.y - 1)
                {
                    tile.Connections.Add(mMap[x][y + 1]);
                }
            }
        }
    }

    private float Distance(EnvironmentTile a, EnvironmentTile b)
    {
        // Use the length of the connection between these two nodes to find the distance, this 
        // is used to calculate the local goal during the search for a path to a location
        float result = float.MaxValue;
        EnvironmentTile directConnection = a.Connections.Find(c => c == b);
        if (directConnection != null)
        {
            result = TileSize;
        }
        return result;
    }

    private float Heuristic(EnvironmentTile a, EnvironmentTile b)
    {
        // Use the locations of the node to estimate how close they are by line of sight
        // experiment here with better ways of estimating the distance. This is used  to
        // calculate the global goal and work out the best order to prossess nodes in
        //return Vector3.Distance(a.Position, b.Position);
        return (b.Position - a.Position).sqrMagnitude;
    }

    public void GenerateWorld(int floorNumber)
    {
        CurrentFloor = floorNumber;

        Generate();
    }

    public void CleanUpWorld()
    {
        if (mMap != null)
        {
            for (int x = 0; x < Size.x; ++x)
            {
                for (int y = 0; y < Size.y; ++y)
                {
                    if (mMap[x][y] != null) Destroy(mMap[x][y].gameObject);
                }
            }

            if (mObjects.Count > 0)
            {
                for (int i = 0; i < mObjects.Count; i++)
                {
                    if (mObjects[i] != null) Destroy(mObjects[i].gameObject);
                }
                mObjects.Clear();
            }

            if (mMonsters.Count > 0)
            {
                for (int i = 0; i < mMonsters.Count; i++)
                {
                    if (mMonsters[i] != null) Destroy(mMonsters[i].gameObject);
                }
                mMonsters.Clear();
            }
        }
    }

    public List<EnvironmentTile> Solve(EnvironmentTile begin, EnvironmentTile destination)
    {
        List<EnvironmentTile> result = null;
        if (begin != null && destination != null)
        {
            // Don't proceed if destination is occupied
            if (!destination.IsAccessible) return null;

            // Nothing to solve if there is a direct connection between these two locations
            EnvironmentTile directConnection = begin.Connections.Find(c => c == destination);
            if (directConnection == null)
            {
                // Set all the state to its starting values
                mToBeTested.Clear();

                for( int count = 0; count < mAll.Count; ++count )
                {
                    mAll[count].Parent = null;
                    mAll[count].Global = float.MaxValue;
                    mAll[count].Local = float.MaxValue;
                    mAll[count].Visited = false;
                }

                // Setup the start node to be zero away from start and estimate distance to target
                EnvironmentTile currentNode = begin;
                currentNode.Local = 0.0f;
                currentNode.Global = Heuristic(begin, destination);

                // Maintain a list of nodes to be tested and begin with the start node, keep going
                // as long as we still have nodes to test and we haven't reached the destination
                mToBeTested.Add(currentNode);

                while (mToBeTested.Count > 0 && currentNode != destination)
                {
                    // Begin by sorting the list each time by the heuristic
                    mToBeTested.Sort((a, b) => (int)(a.Global - b.Global));

                    // Remove any tiles that have already been visited
                    mToBeTested.RemoveAll(n => n.Visited);

                    // Check that we still have locations to visit
                    if (mToBeTested.Count > 0)
                    {
                        // Mark this note visited and then process it
                        currentNode = mToBeTested[0];
                        currentNode.Visited = true;

                        // Check each neighbour, if it is accessible and hasn't already been 
                        // processed then add it to the list to be tested 
                        for (int count = 0; count < currentNode.Connections.Count; ++count)
                        {
                            EnvironmentTile neighbour = currentNode.Connections[count];

                            if (!neighbour.Visited && neighbour.IsAccessible)
                            {
                                mToBeTested.Add(neighbour);
                            }

                            // Calculate the local goal of this location from our current location and 
                            // test if it is lower than the local goal it currently holds, if so then
                            // we can update it to be owned by the current node instead 
                            float possibleLocalGoal = currentNode.Local + Distance(currentNode, neighbour);
                            if (possibleLocalGoal < neighbour.Local)
                            {
                                neighbour.Parent = currentNode;
                                neighbour.Local = possibleLocalGoal;
                                neighbour.Global = neighbour.Local + Heuristic(neighbour, destination);
                            }
                        }
                    }
                }

                // Build path if we found one, by checking if the destination was visited, if so then 
                // we have a solution, trace it back through the parents and return the reverse route
                if (destination.Visited)
                {
                    result = new List<EnvironmentTile>();
                    EnvironmentTile routeNode = destination;

                    while (routeNode.Parent != null)
                    {
                        result.Add(routeNode);
                        routeNode = routeNode.Parent;
                    }
                    result.Add(routeNode);
                    result.Reverse();

                    //Debug.LogFormat("Path Found: {0} steps {1} long", result.Count, destination.Local);
                }
                else
                {
                    //Debug.LogWarning("Path Not Found");
                }
            }
            else
            {
                result = new List<EnvironmentTile>();
                result.Add(begin);
                result.Add(destination);
                //Debug.LogFormat("Direct Connection: {0} <-> {1} {2} long", begin, destination, TileSize);
            }
        }
        else
        {
            Debug.LogWarning("Cannot find path for invalid nodes");
        }

        mLastSolution = result;

        return result;
    }

    public List<EnvironmentTile> GetPatrolRoute(EnvironmentTile begin, ref EnvironmentTile destination, SRoom room)
    {
        List<EnvironmentTile> route = null;
        List<EnvironmentTile> checkedTiles = new List<EnvironmentTile>();
        if (begin != null)
        {
            destination = begin;
            int it = 0; // Limit the number of searches

            // Pick a random position in the room and try to get a path to it until an accessiible path is found
            do
            {
                while ((destination == begin || !destination.IsAccessible))
                {
                    destination = mMap[Random.Range(room.posX, room.posX + room.sizeX)][Random.Range(room.posY, room.posY + room.sizeY)];
                    it++;
                    if (it >= 50) break;
                }
                if (it < 50 && !checkedTiles.Contains(destination))
                {
                    route = Solve(begin, destination);
                    checkedTiles.Add(destination);
                }
                it++;
            }
            while (route == null && it < 50);
        }

        return route;
    }

    public List<EnvironmentTile> GoNearTarget(EnvironmentTile begin, EnvironmentTile targetPos)
    {
        List<EnvironmentTile> route = new List<EnvironmentTile>();

        if (begin != null && targetPos != null)
        {
            List<EnvironmentTile> nearTiles = new List<EnvironmentTile>();
            List<EnvironmentTile> checkedTiles = new List<EnvironmentTile>();

            nearTiles.Add(targetPos);
            int it = 0; // Limit the number of searches

            do
            {
                route = null;

                for (int i = 0; i < nearTiles[0].Connections.Count; i++)
                {
                    if (!nearTiles.Contains(nearTiles[0].Connections[i]) && !checkedTiles.Contains(nearTiles[0].Connections[i])) nearTiles.Add(nearTiles[0].Connections[i]);
                }
                checkedTiles.Add(nearTiles[0]);
                nearTiles.RemoveAt(0);

                for (int i = 0; i < nearTiles.Count; i++) nearTiles[i].Global = Heuristic(nearTiles[i], targetPos) + Heuristic(nearTiles[i], begin);
                nearTiles.Sort((a, b) => (int)(a.Global - b.Global));

                for (int i = 0; i < nearTiles.Count && route == null; i++)
                    if (nearTiles[i].IsAccessible)
                    {
                        route = Solve(begin, nearTiles[i]);
                    }
                it++;
            }
            while (route == null && it < 30);

            // If the new position isn't better than the current position, don't move
            if (route != null)
            {
                if (Heuristic(route[route.Count - 1], targetPos) >= Heuristic(begin, targetPos)) route = null; // Stay in place if the new position isn't closer than the current one
            }
        }

        return route;
    }
}
