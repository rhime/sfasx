﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectPickup : MonoBehaviour
{
    enum EPickupType
    {
        AttackUp,
        DefenseUp,
        Soul
    }

    [SerializeField] EPickupType type;

    Player Player;
    private TaskController TaskController;
    private Portal Portal;
    
    private bool collected = false;

    private void Start()
    {
        Player = GameObject.FindWithTag("Player").GetComponent<Player>();

        TaskController = FindObjectOfType<TaskController>();
        Portal = FindObjectOfType<Portal>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!collected && other.tag == "Player")
        {
            collected = true;

            switch (type)
            {
                case EPickupType.AttackUp:
                {
                    Player.AttackUp(1);
                    break;
                }
                case EPickupType.DefenseUp:
                {
                    Player.DefenseUp(1);
                    break;
                }
                case EPickupType.Soul:
                {
                    if (TaskController.IncreaseProgress()) Portal.Activate();
                    break;
                }
            }

            Destroy(gameObject);
        }
    }
}
