﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSight : MonoBehaviour
{
    [SerializeField] private float SightRange;
    private Vector3 up = new Vector3(0, 1, 0); // Used to avoid raycasting at the floor

    private void OnTriggerStay(Collider other)
    {
        // Check if player can be noticed while the monster is patrolling
        if (GetComponentInParent<Monster>().monsterState == Monster.EMonsterState.Patrol && other.tag == "Player")
        {
            if (other.GetComponentInParent<Character>().isRunning)
            {
                // Apparently monsters can see with their ears
                GetComponentInParent<Monster>().GoAlert(other.GetComponentInParent<Character>());
            }
            else
            {
                // If the player is being sneaky they can only be seen if they are in the cone of sight
                Vector3 direction = (other.transform.position - transform.position).normalized;
                float dot = Vector3.Dot(direction, transform.forward);
                if (dot > SightRange)
                {
                    Ray sightRay = new Ray(transform.position + up, direction);
                    RaycastHit hitPoint;

                    if (Physics.Raycast(sightRay, out hitPoint, Mathf.Infinity))
                    {
                        if (hitPoint.collider.tag == "RaycastPlayer" || hitPoint.collider.tag == "Player")
                        {
                            GetComponentInParent<Monster>().GoAlert(other.GetComponentInParent<Character>(), false);
                        }
                    }
                }
            }
        }
    }
}