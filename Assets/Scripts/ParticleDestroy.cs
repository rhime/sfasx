﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroy : MonoBehaviour
{
	private ParticleSystem particleSystem; 
	private void Start()
	{
		particleSystem = this.GetComponent<ParticleSystem>();
	}

	void OnEnable()
	{
		StartCoroutine("CheckIfAlive");
	}

	IEnumerator CheckIfAlive()
	{
		while (true && particleSystem != null)
		{
			yield return new WaitForSeconds(0.5f);
			if (!particleSystem.IsAlive(true)) GameObject.Destroy(this.gameObject);
		}
	}
}