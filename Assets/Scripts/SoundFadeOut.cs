﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SoundFadeOut
{
    public static IEnumerator FadeOut(AudioSource audio, float fadeSpeed)
    {
        float startVolume = audio.volume;

        while (audio.volume > 0)
        {
            float volume = audio.volume;
            Mathf.SmoothDamp(audio.volume, 0, ref volume, fadeSpeed);
            audio.volume = volume;
            yield return null;
        }

        audio.Stop();
        audio.volume = startVolume;
    }
}
